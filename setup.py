import os
from setuptools import setup, find_packages

here = os.path.abspath(os.path.dirname(__file__))

setup(name='imgtools',
    version='0.1',
    description='Image processing algorithm in OpenCL',
    url='http://mweigert@bitbucket.org/mweigert/imgtools',
    author='Martin Weigert',
    author_email='mweigert@mpi-cbg.de',
    license='MIT',
    packages=find_packages(),
    install_requires=[
        'numpy',"pyopencl","Pillow","pyfft"
    ],

    package_data={"imgtools":['convolve/kernels/*','steerable/kernels/*','imgutils/images/*.png','imgutils/images/*.jpg','imgutils/images/*.tif','ocl_fft/*.cl'],
                  # "imgtools":['steerable/kernels/*'],
                  # "imgtools":['imgutils/images/*']
                  }

)



print find_packages()
