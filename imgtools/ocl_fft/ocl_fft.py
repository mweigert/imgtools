from pyfft.cl import Plan
import pyopencl.array as cl_array
import PyOCL
import numpy as np


import imgtools

def absPath(myPath):
    """ Get absolute path to resource, works for dev and for PyInstaller """
    import sys, os

    try:
        # PyInstaller creates a temp folder and stores path in _MEIPASS
        base_path = sys._MEIPASS
        logger.debug("found MEIPASS: %s "%os.path.join(base_path, os.path.basename(myPath)))

        return os.path.join(base_path, os.path.basename(myPath))
    except Exception:
        base_path = os.path.abspath(os.path.dirname(__file__))
        return os.path.join(base_path, myPath)


def ocl_fft(data = None,dev = None, plan = None):
    return _ocl_fft(data,False, dev, plan)

def ocl_ifft(data = None,dev = None, plan = None):
    return _ocl_fft(data,True, dev, plan)

def _ocl_fft(data,inverse = False, dev = None, plan = None):
    oldShape = data.shape
    data = pad_to_power2(data)

    if dev is None:
        dev = PyOCL.OCLDevice()

    if plan is None:
        plan = Plan(data.shape, queue = dev.queue)
    
    bufIn = cl_array.to_device(dev.queue,data.astype(np.complex64))
    bufOut = cl_array.empty(dev.queue,data.shape,np.complex64)

    plan.execute(bufIn.data,bufOut.data, inverse = inverse)

    return bufOut.get()


def _is_power2(n):
    return _next_power_of_2(n) == n

def _next_power_of_2(n):
    return int(2**np.ceil(np.log2(n)))

def pad_to_power2(data, mode="constant"):
    if np.all([_is_power2(n) for n in data.shape]):
        return data
    else:
        return imgtools.pad_to_shape(data,[_next_power_of_2(n) for n in data.shape],mode)

def ocl_convolve(data, h,dev = None):
    """ convolving via opencl fft

    data and h must have the same size
    """

    if dev is None:
        dev = PyOCL.OCLDevice()

    if data.shape != h.shape:
        raise ValueError("data and kernel must have same size! %s vs %s "%(str(data.shape),str(h.shape)))


    oldShape = data.shape

    data = pad_to_power2(data)
    h = imgtools.pad_to_shape(h,data.shape)

    fftPlan = Plan(data.shape, queue = dev.queue)

    proc = PyOCL.OCLProcessor(dev,absPath("ocl_kernels.cl"),"-I/Users/mweigert/python/pyopencl/build/lib.macosx-10.9-x86_64-2.7/pyopencl/cl/")

    dataBuf = cl_array.to_device(dev.queue,data.astype(np.complex64))
    hBuf = cl_array.to_device(dev.queue,h.astype(np.complex64))
    resBuf = cl_array.empty(dev.queue,data.shape,np.complex64)



    fftPlan.execute(dataBuf.data)
    fftPlan.execute(hBuf.data)

    proc.runKernel("multiply_complex",(dataBuf.size,),None,
                   dataBuf.data,hBuf.data,resBuf.data)

    fftPlan.execute(resBuf.data,inverse=True)

    return imgtools.pad_to_shape(resBuf.get(),oldShape)



def test_fft():
    x = np.random.uniform(0,1,(30,40,50))

    x = imgtools.pad_to_power2(x)
    x_f = ocl_fft(x)

    y = ocl_ifft(x_f)

    print "ocl_fft difference: ",np.mean((np.abs(y)-np.abs(x))**2)
    assert(np.allclose(np.abs(y),np.abs(x),rtol=1.e-3,atol=1.e-3))




if __name__ == '__main__':


    test_fft()
    # from imgtools import ZYX, blur_psf
    # from time import time

    # data = np.zeros((65,)*3)

    # data[50,50,50] = 1.
    # data[20,20,20] = 1.

    # h = blur_psf(data.shape,4)

    # dev= PyOCL.OCLDevice()


    # start = time()
    # y = ocl_convolve(data,np.fft.fftshift(h))

    # dev.queue.finish()
    # print "%.3f ms"%(1000.*(time()-start))


    # x = 10.*np.sin(10.*X)

    # x += 0.* np.random.uniform(0,1,x.shape)


    # x_f = ocl_fft(x)

    # y = ocl_ifft(x_f)
