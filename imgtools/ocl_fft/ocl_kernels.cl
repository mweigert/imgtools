#include <pyopencl-complex.h>



__kernel void multiply_complex(__global cfloat_t* a,__global cfloat_t* b,__global cfloat_t* output){

  uint i = get_global_id(0);
  output[i] = cfloat_mul(a[i], b[i]);
 
}

