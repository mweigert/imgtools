

from numpy import ones, float32
from imgtools import convolve2, test_images, SteerableFilter3

# a convolution

print "checking convolutions...."


data = ones((100,100),dtype=float32)
h = ones((10,10),dtype=float32)

convolve2(data,h)

print "...done"


# a steerable filter
print "checking steerable...."


s = SteerableFilter3()

s.run(ones((100,100,100),dtype=float32), sigma = 4., Ncut=1)

print "...done"
