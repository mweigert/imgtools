
// central difference gradients
__kernel void gradient_norm(__global float * input,__global float * output,const int Nx, const int Ny, const int Nz){

  int i = get_global_id(0);
  int j = get_global_id(1);
  int k = get_global_id(2);

  float res = 0.f;
  float part;
  int n1, n2;

  // x
  n1 = clamp(i+1,0,Nx-1);
  n2 = clamp(i-1,0,Nx-1);

  part = .5f*(input[n1+Nx*j+Nx*Ny*k]-input[n2+Nx*j+Nx*Ny*k]);
  res += part*part;

  // y
  n1 = clamp(j+1,0,Ny-1);
  n2 = clamp(j-1,0,Ny-1);

  part = .5f*(input[i+Nx*n1+Nx*Ny*k]-input[i+Nx*n2+Nx*Ny*k]);
  res += part*part;


  // z
  n1 = clamp(k+1,0,Nz-1);
  n2 = clamp(k-1,0,Nz-1);

  part = .5f*(input[i+Nx*j+Nx*Ny*n1]-input[i+Nx*j+Nx*Ny*n2]);
  res += part*part;
  
  output[i+Nx*j+Nx*Ny*k] = sqrt(res);
 
}



// histogram of floats
__kernel void histogram(__global float * input,__constant float *bins,volatile __global int * output,const int N,const int Nbins){

  int i = get_global_id(0);

  float val = input[i];

  int pos;
  for (pos = 0; pos < Nbins; ++pos)
	if (bins[pos]>=val)
	  break;


  if ((val>=bins[0]) &&(val<=bins[Nbins-1]))
	  atomic_add(&output[pos],1);
 
}

#define LOCALSIZE 512

// center of mass
__kernel void center_of_mass(__global float * input,
							 __global float * rx,
							 __global float * ry,
							 __global float * rz,
							 __global float * rm,							 
							 const int Nx, const int Ny, const int Nz){

  int i0 = get_global_id(0);
  int iLoc = get_local_id(0);
  int iGroup = get_group_id(0);

  __local float4 partialSums[LOCALSIZE];

  float val = input[i0];

  int i = i0;
  
  partialSums[iLoc].x = val*(i%Nx);
  i = i/Nx;
  partialSums[iLoc].y = val*(i%Ny);
  i = i/Ny;
  partialSums[iLoc].z = val*(i%Nz);
  
  partialSums[iLoc].w = val;


  barrier(CLK_LOCAL_MEM_FENCE);
  
  //sum all up
  
  if (iLoc==0){

	float4 res = 0.f;
	for (int n = 0; n < LOCALSIZE; ++n){
	  res += partialSums[n];
	}
	rx[iGroup] = res.x;
	ry[iGroup] = res.y;
	rz[iGroup] = res.z;
	rm[iGroup] = res.w;

  }
 
}

#define LSIZE_IMG 8
#define LSIZE_IMG_ALL 512

// center of mass
__kernel void center_of_mass_img(__read_only image3d_t input,
							 __global float * rx,
							 __global float * ry,
							 __global float * rz,
							 __global float * rm){

  
  const sampler_t sampler = CLK_NORMALIZED_COORDS_FALSE |	CLK_ADDRESS_CLAMP_TO_EDGE |	CLK_FILTER_NEAREST ;

  
  int i0 = get_global_id(0);
  int j0 = get_global_id(1);
  int k0 = get_global_id(2);

  int iLoc = get_local_id(0);
  int jLoc = get_local_id(1);
  int kLoc = get_local_id(2);

  int iGroup = get_group_id(0);
  int jGroup = get_group_id(1);
  int kGroup = get_group_id(2);

  int iDim = get_num_groups(0);
  int jDim = get_num_groups(1);
  int kDim = get_num_groups(2);

  __local float4 partialSums[LSIZE_IMG][LSIZE_IMG][LSIZE_IMG];

  float val = read_imagef(input,sampler,(float4)(i0,j0,k0,0.f)).x;

  int i = i0;
  
  partialSums[iLoc][jLoc][kLoc].x = val*i0;

  partialSums[iLoc][jLoc][kLoc].y = val*j0;

  partialSums[iLoc][jLoc][kLoc].z = val*k0;
  
  partialSums[iLoc][jLoc][kLoc].w = val;


  barrier(CLK_LOCAL_MEM_FENCE);
  
  //sum all up
  
  if (iLoc==0){

	float4 res = 0.f;
	for (int i2 = 0; i2 < LSIZE_IMG; ++i2)
	  for (int j2 = 0; j2 < LSIZE_IMG; ++j2)
		for (int k2 = 0; k2 < LSIZE_IMG; ++k2)
		  res += partialSums[i2][j2][k2];
	
	rx[iGroup+jGroup*iDim+kGroup*iDim*jDim] = res.x;
	ry[iGroup+jGroup*iDim+kGroup*iDim*jDim] = res.y;
	rz[iGroup+jGroup*iDim+kGroup*iDim*jDim] = res.z;
	rm[iGroup+jGroup*iDim+kGroup*iDim*jDim] = res.w;

  }
 
}

  // int i = get_global_id(0);
  // int iloc = get_local_id(0);  
  // int igroup = get_group_id(0);

  // __local float parts[LOCSIZE];

  // //copy to shared
  // parts[iloc] = input[i];

  // barrier(CLK_LOCAL_MEM_FENCE);


  // //reduce partially
  // for (int n = 1; n < LOCSIZE; n*=2){
  // 	int k = 2*n*iloc;
	
  // 	if (k < LOCSIZE)
  // 	  parts[k] += parts[k+n];

	
  // 	barrier(CLK_LOCAL_MEM_FENCE);

  // }

  // if (iloc==0)
  // 	output[igroup] = parts[0];  


// center of mass now with better partoal reduce
__kernel void center_of_mass_img2(__read_only image3d_t input,
							 __global float * rx,
							 __global float * ry,
							 __global float * rz,
							 __global float * rm){

  
  const sampler_t sampler = CLK_NORMALIZED_COORDS_FALSE |	CLK_ADDRESS_CLAMP_TO_EDGE |	CLK_FILTER_NEAREST ;

  
  int i0 = get_global_id(0);
  int j0 = get_global_id(1);
  int k0 = get_global_id(2);

  int iLoc = get_local_id(0);
  int jLoc = get_local_id(1);
  int kLoc = get_local_id(2);

  int iloc = iLoc+LSIZE_IMG*jLoc+LSIZE_IMG*LSIZE_IMG*kLoc;

  int iGroup = get_group_id(0);
  int jGroup = get_group_id(1);
  int kGroup = get_group_id(2);


  int iDim = get_num_groups(0);
  int jDim = get_num_groups(1);
  int kDim = get_num_groups(2);

  __local float4 parts[LSIZE_IMG_ALL];

  float val = read_imagef(input,sampler,(float4)(i0,j0,k0,0.f)).x;

  int i = i0;
  
  parts[iLoc+LSIZE_IMG*jLoc+LSIZE_IMG*LSIZE_IMG*kLoc] = val*(float4)(i0,j0,k0,1.f);

  barrier(CLK_LOCAL_MEM_FENCE);
  
  //sum all up... reduce partially
  for (int n = 1; n < LSIZE_IMG_ALL; n*=2){
	int m = 2*n*iloc;
	
	if (m < LSIZE_IMG_ALL)
	  parts[m] += parts[m+n];

	
	barrier(CLK_LOCAL_MEM_FENCE);

  }

  
  if (iLoc+jLoc+kLoc==0){

  	rx[iGroup+jGroup*iDim+kGroup*iDim*jDim] = parts[0].x;
  	ry[iGroup+jGroup*iDim+kGroup*iDim*jDim] = parts[0].y;
  	rz[iGroup+jGroup*iDim+kGroup*iDim*jDim] = parts[0].z;
  	rm[iGroup+jGroup*iDim+kGroup*iDim*jDim] = parts[0].w;

  }
 
}


// center of mass  even better
__kernel void center_of_mass_img3(__read_only image3d_t input,
							 __global float * rx,
							 __global float * ry,
							 __global float * rz,
							 __global float * rm){

  
  const sampler_t sampler = CLK_NORMALIZED_COORDS_FALSE |	CLK_ADDRESS_CLAMP_TO_EDGE |	CLK_FILTER_NEAREST ;

  
  int i0 = get_global_id(0);
  int j0 = get_global_id(1);
  int k0 = get_global_id(2);

  int iLoc = get_local_id(0);
  int jLoc = get_local_id(1);
  int kLoc = get_local_id(2);

  int iloc = iLoc+LSIZE_IMG*jLoc+LSIZE_IMG*LSIZE_IMG*kLoc;

  int iGroup = get_group_id(0);
  int jGroup = get_group_id(1);
  int kGroup = get_group_id(2);


  int iDim = get_num_groups(0);
  int jDim = get_num_groups(1);
  int kDim = get_num_groups(2);

  __local float4 parts[LSIZE_IMG_ALL];

  float val = read_imagef(input,sampler,(float4)(i0,j0,k0,0.f)).x;

  int i = i0;
  
  parts[iLoc+LSIZE_IMG*jLoc+LSIZE_IMG*LSIZE_IMG*kLoc] = val*(float4)(i0,j0,k0,1.f);

  barrier(CLK_LOCAL_MEM_FENCE);


  //reduce partially
  for (int n = LSIZE_IMG_ALL/2; n >0; n >>=1){
	if (iloc < n)
	  parts[iloc] += parts[iloc+n];

	
	barrier(CLK_LOCAL_MEM_FENCE);

  }

  if (iLoc+jLoc+kLoc==0){

  	rx[iGroup+jGroup*iDim+kGroup*iDim*jDim] = parts[0].x;
  	ry[iGroup+jGroup*iDim+kGroup*iDim*jDim] = parts[0].y;
  	rz[iGroup+jGroup*iDim+kGroup*iDim*jDim] = parts[0].z;
  	rm[iGroup+jGroup*iDim+kGroup*iDim*jDim] = parts[0].w;

  }
 
}

