import pyopencl.array as cl_array
import PyOCL
import numpy as np

import imgtools

def absPath(myPath):
    """ Get absolute path to resource, works for dev and for PyInstaller """
    import sys, os

    try:
        # PyInstaller creates a temp folder and stores path in _MEIPASS
        base_path = sys._MEIPASS
        logger.debug("found MEIPASS: %s "%os.path.join(base_path, os.path.basename(myPath)))

        return os.path.join(base_path, os.path.basename(myPath))
    except Exception:
        base_path = os.path.abspath(os.path.dirname(__file__))
        return os.path.join(base_path, myPath)

def center_of_mass(data, dev= None, proc = None):
    """returns the center of mass of data in relative coordinates [0...1]"""
    
    if dev is None:
        dev= PyOCL.OCLDevice()

    if proc is None:
        proc = PyOCL.OCLProcessor(dev,absPath("algos_kernels.cl"))

    localSize = 512

    Nz,Ny,Nx = data.shape

    bufSize = int(np.ceil(data.size/localSize))
    
    bufIn = cl_array.to_device(dev.queue,data.astype(np.float32))
    bufsOut = [cl_array.empty(dev.queue,bufSize,np.float32) for i in range(4)]

    import time
    start = time.time()


    proc.runKernel("center_of_mass",(bufSize*localSize,),(localSize,),bufIn.data,
                   bufsOut[0].data,bufsOut[1].data,bufsOut[2].data,bufsOut[3].data,
				   np.int32(Nx),np.int32(Ny),np.int32(Nz))

    dev.queue.finish()
    print "%.4f ms"%(1000.*(time.time()-start))

    sums = np.array([sum(b.get()) for b in bufsOut])

    
    return sums[:3]/sums[-1]



def center_of_mass_img_old(data, dev= None, proc = None):
    """returns the center of mass of data in relative coordinates [0...1]"""
    
    if dev is None:
        dev= PyOCL.OCLDevice()

    if proc is None:
        proc = PyOCL.OCLProcessor(dev,absPath("algos_kernels.cl"))

    localSize = 8

    Nz,Ny,Nx = data.shape


    padShape = [int(np.ceil(1.*d/localSize)*localSize) for d in data.shape[::-1]]

    
    locShape = [d/localSize for d in padShape]
    img = dev.createImage_like(data.astype(np.float32))
    
    bufsOut = [cl_array.empty(dev.queue,np.prod(locShape),np.float32) for i in range(4)]


    
    dev.writeImage(img,data.astype(np.float32))

    dev.queue.finish()
    
    import time
    start = time.time()


    proc.runKernel("center_of_mass_img",padShape,(localSize,)*3,img,
                   bufsOut[0].data,bufsOut[1].data,bufsOut[2].data,bufsOut[3].data)

    dev.queue.finish()
    print "runtime for %s: %.4f ms"%(str(data.shape),(1000.*(time.time()-start)))

    outs = [b.get() for b in bufsOut]

    sums = np.array([sum(b.get()) for b in bufsOut])
        
    res = sums[:3]/sums[-1]
    return res

def center_of_mass_img(data, dev= None, proc = None):
    """returns the center of mass of data in relative coordinates [0...1]"""
    
    if dev is None:
        dev= PyOCL.OCLDevice()

    if proc is None:
        proc = PyOCL.OCLProcessor(dev,absPath("algos_kernels.cl"))

    localSize = 8

    Nz,Ny,Nx = data.shape


    padShape = [int(np.ceil(1.*d/localSize)*localSize) for d in data.shape[::-1]]

    
    locShape = [d/localSize for d in padShape]
    img = dev.createImage_like(data.astype(np.float32))
    
    bufsOut = [cl_array.empty(dev.queue,np.prod(locShape),np.float32) for i in range(4)]


    
    dev.writeImage(img,data.astype(np.float32))

    dev.queue.finish()
    
    import time
    start = time.time()


    proc.runKernel("center_of_mass_img3",padShape,(localSize,)*3,img,
                   bufsOut[0].data,bufsOut[1].data,bufsOut[2].data,bufsOut[3].data)

    dev.queue.finish()
    print "runtime for %s: %.4f ms"%(str(data.shape),(1000.*(time.time()-start)))

    outs = [b.get() for b in bufsOut]

    sums = np.array([sum(b.get()) for b in bufsOut])
        
    res = sums[:3]/sums[-1]
    return res

def test_centers():
    dev = PyOCL.OCLDevice()

    for N in range(50,260,20):
        #linear ramp
        data = .5*(1.+imgtools.ZYX(N)[2])
        for expon in range(5):
            center = center_of_mass_img(data**expon, dev = dev)
            center0 = N*np.array([(expon+1.)/(expon+2.),.5,.5])
            print "%s vs  %s"%(center0.round(3), center.round(3))
            print "rms: \t %s"%(np.mean((center-center0)**2))
        

if __name__ == '__main__':


    # test_centers()

    # dev = PyOCL.OCLDevice()


    data = .5*(1.+imgtools.ZYX(256)[2])

    data = np.ones((256,256,256),np.float32)
    print center_of_mass_img(data)

