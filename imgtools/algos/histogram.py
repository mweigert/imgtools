import pyopencl.array as cl_array
import PyOCL
import numpy as np


import imgtools

def absPath(myPath):
    """ Get absolute path to resource, works for dev and for PyInstaller """
    import sys, os

    try:
        # PyInstaller creates a temp folder and stores path in _MEIPASS
        base_path = sys._MEIPASS
        logger.debug("found MEIPASS: %s "%os.path.join(base_path, os.path.basename(myPath)))

        return os.path.join(base_path, os.path.basename(myPath))
    except Exception:
        base_path = os.path.abspath(os.path.dirname(__file__))
        return os.path.join(base_path, myPath)


def histogram(data,bins, dev = None):
    if dev is None:
        dev= PyOCL.OCLDevice()
      
    proc = PyOCL.OCLProcessor(dev,absPath("algos_kernels.cl"))
  
    
    bufIn = cl_array.to_device(dev.queue,data.astype(np.float32))
    bufBins = cl_array.to_device(dev.queue,bins.astype(np.float32))
    bufOut = cl_array.empty(dev.queue,bins.size,np.int32)

    proc.runKernel("histogram",(data.size,),None,
                   bufIn.data,bufBins.data,bufOut.data,
                   np.int32(data.size),np.int32(bins.size))

    return bufOut.get()
    

    

if __name__ == '__main__':

    from imgtools import ZYX

    data = np.linspace(0,1,100)

    data = np.random.normal(0,1,10000)

    bins  = np.linspace(-2.,2.,50)

    print histogram(data,bins)
