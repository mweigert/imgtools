
#define localSize 8

__kernel void tenengrad_2d_sobel(__read_only image2d_t input,__global float* output,const int stride, const int reducedStride, __constant float* sobelKernel, const int Nsobel){

  
  const sampler_t sampler = CLK_NORMALIZED_COORDS_FALSE |	CLK_ADDRESS_CLAMP |	CLK_FILTER_NEAREST ;

  int i = get_global_id(0);
  int j = get_global_id(1);

  int iLoc = get_local_id(0);
  int jLoc = get_local_id(1);

  int iGroupLoc = get_group_id(0);
  int jGroupLoc = get_group_id(1);

  __local float partialResult[localSize][localSize];
  
  //Gx, Gy
  float Gx = 0.f, Gy = 0.f;
  
  float h = .5f*(Nsobel-1);

  
  for (int n = 0; n < Nsobel; ++n){
	float sobelVal = sobelKernel[n];
	for (int m = 0; m < Nsobel; ++m){
	  Gx += (m-h)*sobelVal*read_imagef(input,sampler,(float2)(i+n-h,j+m-h)).x;
	  Gy += (m-h)*sobelVal*read_imagef(input,sampler,(float2)(i+m-h,j+n-h)).x;
	}
  }
  
  float tenengrad_measure = sqrt(Gx*Gx+Gy*Gy);

  partialResult[iLoc][jLoc] = tenengrad_measure;


  barrier(CLK_LOCAL_MEM_FENCE);

  //now sum
  if ((iLoc==0) &&(jLoc==0)){
	float res = 0.f;
	for (int n = 0; n < localSize; ++n)
	  for (int m = 0; m < localSize; ++m)
		res += partialResult[n][m];

		
	output[iGroupLoc+reducedStride*jGroupLoc] = res/localSize/localSize;

  }
}


__kernel void tenengrad_3d_sobel(__read_only image3d_t input,__global float* output, const int reducedStride2,const int reducedStride1, __constant float* sobelKernel, const int Nsobel){

  
  const sampler_t sampler = CLK_NORMALIZED_COORDS_FALSE |	CLK_ADDRESS_CLAMP_TO_EDGE |	CLK_FILTER_NEAREST ;

  int i = get_global_id(0);
  int j = get_global_id(1);
  int k = get_global_id(2);

  int iLoc = get_local_id(0);
  int jLoc = get_local_id(1);
  int kLoc = get_local_id(2);

  int iGroupLoc = get_group_id(0);
  int jGroupLoc = get_group_id(1);
  int kGroupLoc = get_group_id(2);

  __local float partialResult[localSize][localSize][localSize];
  
  //Gx, Gy
  float Gx = 0.f, Gy = 0.f,  Gz = 0.f;
  
  float h = .5f*(Nsobel-1);

  for (int n = 0; n < Nsobel; ++n){
	float sob_n = sobelKernel[n];
	for (int m = 0; m < Nsobel; ++m){
	  	float sob_m = sobelKernel[m];
	  	for (int p = 0; p < Nsobel; ++p){

		  Gx += (p-h)*sob_m*sob_n*
			read_imagef(input,sampler,(float4)(i+p-h,j+m-h,k+n-h,0.f)).x;

		  Gy += (p-h)*sob_m*sob_n*
			read_imagef(input,sampler,(float4)(i+n-h,j+p-h,k+m-h,0.f)).x;

		  Gz += (p-h)*sob_m*sob_n*
			read_imagef(input,sampler,(float4)(i+m-h,j+n-h,k+p-h,0.f)).x;

		}
	}
  }
  
  float tenengrad_measure = Gx*Gx + Gy*Gy + Gz*Gz;

  // tenengrad_measure = read_imagef(input,sampler,(float4)(i,j,k,0.f)).x;

	
  partialResult[iLoc][jLoc][kLoc] = tenengrad_measure;


  barrier(CLK_LOCAL_MEM_FENCE);

  //now sum
  if ((iLoc==0) && (jLoc==0) && (kLoc==0)){

	float res = 0.f;
	for (int n = 0; n < localSize; ++n)
	  for (int m = 0; m < localSize; ++m)
		for (int p = 0; p < localSize; ++p)
		  res += partialResult[n][m][p];

	output[iGroupLoc+reducedStride1*jGroupLoc+reducedStride2*reducedStride1*kGroupLoc] = res/localSize/localSize/localSize;

  }		
}



__kernel void convolve_sep2d_inplace_float(__global float * input, const int Nx,const int Ny,__constant float * h,const int Nh,const int flag){

  // flag = 1 -> in x axis 
  // flag = 2 -> in y axis 
  // flag = 4 -> in z axis 
  

  int i = get_global_id(0);
  int j = get_global_id(1);



  
  const int dx = flag & 1;
  const int dy = (flag&2)/2;
  // const int dz = (flag&4)/4;

  float res = 0.f;
  int delta = (Nh-1)/2;
  
  for (int p = 0; p < Nh; ++p){

	int i1 = i+dx*(p-delta);
	int j1 = j+dy*(p-delta);
	
	float val = 0.f;
	//clamp!!!!
	if ((i1>=0)&&(j1>=0)&&(i1<Nx)&&(j1<Ny))
	  val = input[i1+Nx*j1];
	
	res += h[p]*val;

  }

  barrier(CLK_GLOBAL_MEM_FENCE);


  
  input[i+Nx*j] = res;
  
  
}

__kernel void convolve_sep2d_float(__global float * input, __global float * output, const int Nx,const int Ny,__constant float * h,const int Nh,const int flag){

  // flag = 1 -> in x axis 
  // flag = 2 -> in y axis 
  // flag = 4 -> in z axis 
  

  int i = get_global_id(0);
  int j = get_global_id(1);



  
  const int dx = flag & 1;
  const int dy = (flag&2)/2;
  // const int dz = (flag&4)/4;

  float res = 0.f;
  int delta = (Nh-1)/2;
  
  for (int p = 0; p < Nh; ++p){

	int i1 = i+dx*(p-delta);
	int j1 = j+dy*(p-delta);
	
	float val = 0.f;

	//clamp to border
	i1 = clamp(i1,0,Nx-1);
	j1 = clamp(j1,0,Ny-1);

	val = input[i1+Nx*j1];
	
	res += h[p]*val;

  }

  output[i+Nx*j] = res;  
 
}



__kernel void convolve_sep3d_inplace_float(__global float * input, const int Nx,const int Ny,const int Nz,__constant float * h,const int Nh,const int flag){

  // flag = 1 -> in x axis 
  // flag = 2 -> in y axis 
  // flag = 4 -> in z axis 
  

  int i = get_global_id(0);
  int j = get_global_id(1);
  int k = get_global_id(2);

  
  const int dx = flag & 1;
  const int dy = (flag&2)/2;
  const int dz = (flag&4)/4;

  float res = 0.f;
  int delta = (Nh-1)/2;

	
  for (int p = 0; p < Nh; ++p){

	int i1 = i+dx*(p-delta);
	int j1 = j+dy*(p-delta);
	int k1 = k+dz*(p-delta);
	
	float val = 0.f;

	//clamp to border
	i1 = clamp(i1,0,Nx-1);
	j1 = clamp(j1,0,Ny-1);
	k1 = clamp(k1,0,Nz-1);

	val = input[i1+Nx*j1+Nx*Ny*k1];
	
	res += h[p]*val;

  }

  barrier(CLK_GLOBAL_MEM_FENCE);

  // if ((i==Nx/2) &&(j==Ny/2) &&(k==Nz/2))
  // 	printf("%.4f   %.4f %.2f %.2f %.2f \n",input[i+Nx*j+Nx*Ny*k],res,(float)dx,(float)dy,(float)dz);

  input[i+Nx*j+Nx*Ny*k] = res;  
 
}





__kernel void convolve_sep3d_float__(__global float * input, __global float * output, const int Nx,const int Ny,const int Nz,__constant float * h,const int Nh,const int flag){

  // flag = 1 -> in x axis 
  // flag = 2 -> in y axis 
  // flag = 4 -> in z axis 
  

  int i = get_global_id(0);
  int j = get_global_id(1);
  int k = get_global_id(2);



  
  const int dx = flag & 1;
  const int dy = (flag&2)/2;
  const int dz = (flag&4)/4;

  float res = 0.f;
  int delta = (Nh-1)/2;
  
  for (int p = 0; p < Nh; ++p){

	int i1 = i+dx*(p-delta);
	int j1 = j+dy*(p-delta);
	int k1 = k+dz*(p-delta);
	
	float val = 0.f;

	//clamp to border
	i1 = clamp(i1,0,Nx-1);
	j1 = clamp(j1,0,Ny-1);
	k1 = clamp(k1,0,Nz-1);

	val = input[i1+Nx*j1+Nx*Ny*k1];
	
	res += h[p]*val;

  }

  output[i+Nx*j+Nx*Ny*k] = res;  
  //output[i+Nx*j+Nx*Ny*k] = i;  
 
}

__kernel void convolve_sep3d_float(__global float * input, __global float * output, const int Nx,const int Ny,const int Nz,__constant float * h,const int Nh,const int flag){

  int i = get_global_id(0);
  int j = get_global_id(1);
  int k = get_global_id(2);

    
  const int dx = flag & 1;
  const int dy = (flag&2)/2;
  const int dz = (flag&4)/4;

  float res = 0.f;
  int delta = (Nh-1)/2;

  for (int p = 0; p < Nh; ++p){

	int i1 = i+dx*(p-delta);
	int j1 = j+dy*(p-delta);
	int k1 = k+dz*(p-delta);
	
	float val = 0.f;

	//clamp to border
	i1 = clamp(i1,0,Nx-1);
	j1 = clamp(j1,0,Ny-1);
	k1 = clamp(k1,0,Nz-1);

	val = input[i1+Nx*j1+Nx*Ny*k1];
	
	res += h[p]*val;

  }


  output[i+Nx*j+Nx*Ny*k] = res;  
 
}




__kernel void downsample(__read_only image3d_t input,__global float* output, const int Nx,const int Ny,const int Nz, const int Ndown){

  
  const sampler_t sampler = CLK_NORMALIZED_COORDS_FALSE |	CLK_ADDRESS_CLAMP_TO_EDGE |	CLK_FILTER_NEAREST ;

  int i = get_global_id(0);
  int j = get_global_id(1);
  int k = get_global_id(2);

  int i0 = Ndown*i;
  int j0 = Ndown*j;
  int k0 = Ndown*k;

  float res = 0.f;

  float dx = .5f*(Ndown-1.);
  
  for (int n = 0; n < Ndown; ++n)
	for (int m = 0; m < Ndown; ++m)
	  for (int p = 0; p < Ndown; ++p)
		// res += read_imagef(input,sampler,
		// 			   (float4)(Ndown*i+n*dx,Ndown*j+m*dx,Ndown*k+p*dx,0.f)).x;
  
		res += read_imagef(input,sampler,
					   (float4)(i0+n,j0+m,k0+p,0.f)).x;
 
  output[i+Nx*j+Nx*Ny*k] = res/Ndown/Ndown/Ndown;
}


__kernel void copy(__global float * input, __global float * output, const int Nx,const int Ny,const int Nz){


 
  int i = get_global_id(0);
  int j = get_global_id(1);
  int k = get_global_id(2);


  output[i+Nx*j+Nx*Ny*k] = input[i+Nx*j+Nx*Ny*k];  
 
}

__kernel void copy_img(__read_only image3d_t input, __global float * output, const int Nx,const int Ny,const int Nz){


  const sampler_t sampler = CLK_NORMALIZED_COORDS_FALSE |	CLK_ADDRESS_CLAMP_TO_EDGE |	CLK_FILTER_NEAREST ;
 
  int i = get_global_id(0);
  int j = get_global_id(1);
  int k = get_global_id(2);

  float res = 0;
  for (int k = 0; k < 3; ++k){
	res += read_imagef(input,sampler,(float4)(i+k,j,k,0.f)).x;
    
  }
  
  output[i+Nx*j+Nx*Ny*k] =  res;  
 
}

