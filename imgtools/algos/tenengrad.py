"""implements the tenegrad filter to measure image focus

see
Krotkov, E.P., 1987. Focusing, Int. Jour. of Computer Vision. 1 (3), 223-237


"""


from numpy import *
import imgtools
import PyOCL
from time import time, sleep


def tenengrad2_old(data,sobel=[1,2,1],localSize = 8,dev=None,proc=None):

    if dev is None:
        dev = PyOCL.OCLDevice()

    if proc is None:
        proc = PyOCL.OCLProcessor(dev,"tenengrad.cl")

    img = dev.createImage_like(data,float32)

    #we might have to pad to make diemnsions divisible by localSize
    paddedShape =  tuple(int(ceil(1.*n/localSize)*localSize) for n in data.shape)

    #the shape of the local sums
    sumShape = tuple(n/localSize for n in paddedShape)

    kSobel = array(sobel).astype(float32)
    sobelBuf = dev.createBuffer(len(kSobel),PyOCL.cl.mem_flags.READ_ONLY,float32)
    dev.writeBuffer(sobelBuf,kSobel)

    buf = dev.createBuffer(prod(sumShape),PyOCL.cl.mem_flags.READ_WRITE,float32)

    dev.writeImage(img,data.astype(float32))

    dev.queue.finish()
    start = time()
    proc.runKernel("tenengrad_2d_sobel",paddedShape[::-1],(localSize,localSize),img,buf,int32(data.shape[1]),int32(sumShape[1]),sobelBuf,int32(len(kSobel)))


    dev.queue.finish()
    print 1000.*(time()-start),"ms"

    out = dev.readBuffer(buf,float32).reshape(sumShape)

    return mean(out)


def tenengrad2(data,NSobel=3,dev=None,proc=None):

    if dev is None:
        dev = PyOCL.OCLDevice()

    if proc is None:
        proc = PyOCL.OCLProcessor(dev,"tenengrad.cl")

    buf = dev.createBuffer(data.size,PyOCL.cl.mem_flags.READ_ONLY,float32)

    dev.writeBuffer(buf,data.flatten().astype(float32))

    h_diff = linspace(-1,1,NSobel)
    h_diff *= 1./sum(h_diff)

    h_smooth = (NSobel-1)-abs(linspace(-(NSobel//2),(NSobel//2),NSobel))
    h_smooth *= 1./sum(h_smooth)

    h_diffBuf = dev.createBuffer(len(h_diff),
                                 PyOCL.cl.mem_flags.READ_ONLY,float32)
    dev.writeBuffer(h_diffBuf,h_diff.astype(float32))

    h_smoothBuf = dev.createBuffer(len(h_smooth),
                                   PyOCL.cl.mem_flags.READ_ONLY,float32)
    dev.writeBuffer(h_smoothBuf,h_smooth.astype(float32))

    bufGx = dev.createBuffer(data.size,PyOCL.cl.mem_flags.READ_WRITE,float32)
    bufGy = dev.createBuffer(data.size,PyOCL.cl.mem_flags.READ_WRITE,float32)

    dev.queue.finish()
    start = time()

    #the Gx component
    proc.runKernel("convolve_sep2d_float",data.shape[::-1],None,buf,bufGx,int32(data.shape[1]),int32(data.shape[0]),h_diffBuf,int32(len(h_diff)),int32(1))
    proc.runKernel("convolve_sep2d_inplace_float",data.shape[::-1],None,bufGx,int32(data.shape[1]),int32(data.shape[0]),h_smoothBuf,int32(len(h_smooth)),int32(2))

    #the Gy component
    proc.runKernel("convolve_sep2d_float",data.shape[::-1],None,buf,bufGy,int32(data.shape[1]),int32(data.shape[0]),h_diffBuf,int32(len(h_diff)),int32(2))
    proc.runKernel("convolve_sep2d_inplace_float",data.shape[::-1],None,bufGy,int32(data.shape[1]),int32(data.shape[0]),h_smoothBuf,int32(len(h_smooth)),int32(1))

    dev.queue.finish()
    print 1000.*(time()-start),"ms"

    outx = dev.readBuffer(bufGx,float32).reshape(data.shape)
    outy = dev.readBuffer(bufGy,float32).reshape(data.shape)

    G = mean(sqrt(outx**2+outy**2))
    return G



def test_tenen2():
    d = pad(imgtools.test_images.fluorescent()-24,10,mode="constant")
    out = tenengrad2(d)
    assert(isclose(out,65.804,atol=1.e-3))



def tenengrad3(data, downSample = 3, NSobel = 3):
    dev = PyOCL.OCLDevice()

    proc = PyOCL.OCLProcessor(dev,"tenengrad.cl")

    img = dev.createImage_like(data,float32)
    dev.writeImage(img,data.astype(float32))

    paddedShape =  tuple(int(ceil(1.*n/downSample)*downSample) for n in data.shape)
    downShape =  tuple(n/downSample for n in paddedShape)

    downBuf = dev.createBuffer(prod(downShape),
                               PyOCL.cl.mem_flags.READ_WRITE,float32)

    start = time()

    proc.runKernel("downsample",downShape[::-1],None,img,downBuf,\
                   int32(downShape[2]),int32(downShape[1]),int32(downShape[0]),\
                   int32(downSample))


    dev.queue.finish()
    print "downsample: %.4f ms"%(1000.*(time()-start))
    start = time()

    h_diff = linspace(-1,1,NSobel)
    h_diff *= 1./(len(h_diff)-1.)


    h_smooth = (NSobel-1)-abs(linspace(-(NSobel//2),(NSobel//2),NSobel))
    h_smooth *= 1./sum(h_smooth)

    h_diffBuf = dev.createBuffer(len(h_diff),
                                 PyOCL.cl.mem_flags.READ_ONLY,float32)
    dev.writeBuffer(h_diffBuf,h_diff.astype(float32))


    h_smoothBuf = dev.createBuffer(len(h_smooth),
                                   PyOCL.cl.mem_flags.READ_ONLY,float32)
    dev.writeBuffer(h_smoothBuf,h_smooth.astype(float32))

    bufGx = dev.createBuffer(prod(downShape),PyOCL.cl.mem_flags.READ_WRITE,float32)
    bufGy = dev.createBuffer(prod(downShape),PyOCL.cl.mem_flags.READ_WRITE,float32)
    bufGz = dev.createBuffer(prod(downShape),PyOCL.cl.mem_flags.READ_WRITE,float32)


    outDown = dev.readBuffer(downBuf,float32).reshape(downShape)

    dev.queue.finish()
    start = time()

    #the Gx component
    proc.runKernel("convolve_sep3d_float",downShape[::-1],None,downBuf,bufGx,
                   int32(downShape[2]),int32(downShape[1]),int32(downShape[0]),
                   h_diffBuf,int32(NSobel),int32(1))


    proc.runKernel("convolve_sep3d_inplace_float",downShape[::-1],None,bufGx,
                   int32(downShape[2]),int32(downShape[1]),int32(downShape[0]),
                   h_smoothBuf,int32(NSobel),int32(2))

    proc.runKernel("convolve_sep3d_inplace_float",downShape[::-1],None,bufGx,
                   int32(downShape[2]),int32(downShape[1]),int32(downShape[0]),
                   h_smoothBuf,int32(NSobel),int32(4))

    #the Gy component
    proc.runKernel("convolve_sep3d_float",downShape[::-1],None,downBuf,bufGy,
                   int32(downShape[2]),int32(downShape[1]),int32(downShape[0]),
                   h_diffBuf,int32(NSobel),int32(2))


    proc.runKernel("convolve_sep3d_inplace_float",downShape[::-1],None,bufGy,
                   int32(downShape[2]),int32(downShape[1]),int32(downShape[0]),
                   h_smoothBuf,int32(NSobel),int32(1))

    proc.runKernel("convolve_sep3d_inplace_float",downShape[::-1],None,bufGy,
                   int32(downShape[2]),int32(downShape[1]),int32(downShape[0]),
                   h_smoothBuf,int32(NSobel),int32(4))

    #the Gz component
    proc.runKernel("convolve_sep3d_float",downShape[::-1],None,downBuf,bufGz,
                   int32(downShape[2]),int32(downShape[1]),int32(downShape[0]),
                   h_diffBuf,int32(NSobel),int32(4))


    proc.runKernel("convolve_sep3d_inplace_float",downShape[::-1],None,bufGz,
                   int32(downShape[2]),int32(downShape[1]),int32(downShape[0]),
                   h_smoothBuf,int32(NSobel),int32(2))

    proc.runKernel("convolve_sep3d_inplace_float",downShape[::-1],None,bufGz,
                   int32(downShape[2]),int32(downShape[1]),int32(downShape[0]),
                   h_smoothBuf,int32(NSobel),int32(1))


    dev.queue.finish()
    print "convolving: %.4f ms"%(1000.*(time()-start))
    start = time()

    outx = dev.readBuffer(bufGx,float32).reshape(downShape)
    outy = dev.readBuffer(bufGy,float32).reshape(downShape)
    outz = dev.readBuffer(bufGz,float32).reshape(downShape)



    G = sqrt(outx**2+outy**2+outz**2)

    return mean(G), G


if __name__ == '__main__':

    from imgtools import blur, blur_psf

    data = imgtools.read3dTiff("/Users/mweigert/Data/synthetics/droso_phantom256.tif")
    rand = random.normal(0,1,data.shape)

    rs = linspace(0,8,20)

    tenengrad3(data,downSample=1,NSobel = 5)

    # def bench(sig, NSobel,downSample):
    #     res = []
    #     outs = []

    #     for r in rs:
    #         y = blur(data,1.+r) + sig*rand
    #         g, out = tenengrad3(y,NSobel=NSobel, downSample=downSample)
    #         res.append(g)
    #         outs.append(y)
    #         print r, g

    #     return res, outs


    # resN1 = [bench(5,n,3)[0] for n in [3,5,7]]
    # resD1 = [bench(5,3,n)[0] for n in [3,5,7]]

    # resN2 = [bench(15,n,3)[0] for n in [3,5,7]]
    # resD2 = [bench(15,3,n)[0] for n in [3,5,7]]
