from pyfft.cl import Plan
import pyopencl.array as cl_array
import PyOCL
import numpy as np


import imgtools

def absPath(myPath):
    """ Get absolute path to resource, works for dev and for PyInstaller """
    import sys, os

    try:
        # PyInstaller creates a temp folder and stores path in _MEIPASS
        base_path = sys._MEIPASS
        logger.debug("found MEIPASS: %s "%os.path.join(base_path, os.path.basename(myPath)))

        return os.path.join(base_path, os.path.basename(myPath))
    except Exception:
        base_path = os.path.abspath(os.path.dirname(__file__))
        return os.path.join(base_path, myPath)

def gradient_norm(data, dev= None, proc = None):
    """returns the l2 norm of the local gradients (central differences)"""
    
    if dev is None:
        dev= PyOCL.OCLDevice()

    if proc is None:
        proc = PyOCL.OCLProcessor(dev,absPath("algos_kernels.cl"))
    
    bufIn = cl_array.to_device(dev.queue,data.astype(np.float32))
    bufOut = cl_array.empty(dev.queue,data.shape,np.float32)


    proc.runKernel("gradient_norm",(Nx,Ny,Nz),None,bufIn.data,bufOut.data,
                   np.int32(Nx),np.int32(Ny),np.int32(Nz))

    return bufOut.get().reshape(data.shape)

    

    


if __name__ == '__main__':

    from imgtools import ZYX, blur_psf

    data = np.linspace(0,1,100)

    data = np.random.normal(0,1,10000)

    bins  = np.linspace(-2.,2.,50)

    print histogram(data,bins)
