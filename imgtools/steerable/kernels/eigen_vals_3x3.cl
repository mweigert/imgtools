/*
  calculates the maximal eigenvalue and the corresponding eigenvector
  of a symmetric 3x3 matrix mat_ij

 */

__kernel void eigen_max(global float* output,
				  const int Nx,
				  const int Ny,
				  const int Nz,
				  __read_only image3d_t mat_11,
				  __read_only image3d_t mat_22,
				  __read_only image3d_t mat_33,
				  __read_only image3d_t mat_12,
				  __read_only image3d_t mat_23,
				  __read_only image3d_t mat_13
				  ){

  
  const sampler_t sampler = CLK_NORMALIZED_COORDS_FALSE |	CLK_ADDRESS_CLAMP_TO_EDGE |	CLK_FILTER_NEAREST ;

  int i0 = get_global_id(0);
  int j0 = get_global_id(1);
  int k0 = get_global_id(2);

  float A11 = read_imagef(mat_11,sampler,(int4)(i0,j0,k0,0)).x;
  float A22 = read_imagef(mat_22,sampler,(int4)(i0,j0,k0,0)).x;
  float A33 = read_imagef(mat_33,sampler,(int4)(i0,j0,k0,0)).x;
  float A12 = read_imagef(mat_12,sampler,(int4)(i0,j0,k0,0)).x;
  float A23 = read_imagef(mat_23,sampler,(int4)(i0,j0,k0,0)).x;
  float A13 = read_imagef(mat_13,sampler,(int4)(i0,j0,k0,0)).x;

  float eig1 = 0.f;
  float eig2 = 0.f;
  float eig3 = 0.f;
  
  float p1 = A12*A12 + A23*A23 + A13*A13;

  
  if (p1<1.e-8){

  	eig1 = max(max(A11,A22),A33);
  	eig2 = min(max(A11,A22),A33);
  	eig3 = min(min(A11,A22),A33);

  }
  else{
	
  	float q = (A11 + A22 + A33)/3.;
  	float p2 = (A11 - q)*(A11 - q) + (A22 - q)*(A22 - q)
  	  + (A33 - q)*(A33 - q) + 2.f * p1;
  	float p = sqrt(p2 / 6);

  	float B11 =  (1 / p) * (A11 - q);
  	float B22 =  (1 / p) * (A22 - q);
  	float B33 =  (1 / p) * (A33 - q);
  	float B12 =  (1 / p) * A12;
  	float B23 =  (1 / p) * A23;
  	float B13 =  (1 / p) * A13;


  	float phi = 0.f;

  	//  r = 0.5 * det(B)

  	float r  = .5*(B11*(B22*B33-B23*B23)-B12*(B12*B33-B23*B13)
  				   +B13*(B12*B23-B22*B13));   
	
  	if (fabs(r) >1.)
  	  if (r<0)
  		phi = M_PI_F / 3.f;
  	  else
  		phi = 0;
  	else
  	  phi = acos(r) / 3.f;
	
  	eig1 = q + 2*p*cos(phi);
  	eig2 = q + 2*p*cos(phi+2.*M_PI_F/3.);
  	eig3 = q + 2*p*cos(phi+4.*M_PI_F/3.);

	
  }

  // now calculate the eigenvector with resp. to the maximal eigenvalue eig1

  // C = A^2 - (eig2+eig3)*A + eig2*eig3*I

  float esum = (eig2+eig3);
  float eprod = eig2*eig3;
  
  float C11 =  A11*A11 + A12*A12 + A13*A13 - esum*A11 + eprod;
  float C22 =  A12*A12 + A22*A22 + A23*A23 - esum*A22 + eprod;
  float C33 =  A13*A13 + A23*A23 + A33*A33 - esum*A33 + eprod;
  float C12 =  A11*A12 + A12*A22 + A13*A23 - esum*A12;
  float C23 =  A12*A13 + A22*A23 + A23*A33 - esum*A23;
  float C13 =  A11*A13 + A12*A23 + A13*A33 - esum*A13;

  float a,b,c;

  // try first column/row
  a = C11;
  b = C12;
  c = C13;

  if (a*a+b*b+c*c<1.e-6){
	a = C12;
	b = C22;
	c = C23;
  }
  if (a*a+b*b+c*c<1.e-6){
	a = C13;
	b = C23;
	c = C33;
  }

  if (a*a+b*b+c*c<1.e-6){
	a = 1.;
	b = 0.;
	c = 0.;
  }

  if (b<0){
	a *= -1.f;
	b *= -1.f;
	c *= -1.f;

  }
	
  float norm = sqrt(a*a+b*b+c*c);
  a *= 1./norm;
  b *= 1./norm;
  c *= 1./norm;


  output[i0 + Nx*j0 + Nx*Ny*k0 + Nx*Ny*Nz*0] = eig1;
  output[i0 + Nx*j0 + Nx*Ny*k0 + Nx*Ny*Nz*1] = a;
  output[i0 + Nx*j0 + Nx*Ny*k0 + Nx*Ny*Nz*2] = b;
  output[i0 + Nx*j0 + Nx*Ny*k0 + Nx*Ny*Nz*3] = c;

  

}

__kernel void eigen_max_buf(global float* output,
				  const int Nx,
				  const int Ny,
				  const int Nz,
				  global float * mat_11,
				  global float * mat_22,
				  global float * mat_33,
				  global float * mat_12,
				  global float * mat_23,
				  global float * mat_13
				  ){

  
  const sampler_t sampler = CLK_NORMALIZED_COORDS_FALSE |	CLK_ADDRESS_CLAMP_TO_EDGE |	CLK_FILTER_NEAREST ;

  int i0 = get_global_id(0);
  int j0 = get_global_id(1);
  int k0 = get_global_id(2);

  const long ind = i0 + Nx*j0 + Nx*Ny*k0;
  
  float A11 = mat_11[ind];
  float A22 = mat_22[ind];
  float A33 = mat_33[ind];
  float A12 = mat_12[ind];
  float A23 = mat_23[ind];
  float A13 = mat_13[ind];

  float eig1 = 0.f;
  float eig2 = 0.f;
  float eig3 = 0.f;
  
  float p1 = A12*A12 + A23*A23 + A13*A13;

  
  if (p1<1.e-8){

  	eig1 = max(max(A11,A22),A33);
  	eig2 = min(max(A11,A22),A33);
  	eig3 = min(min(A11,A22),A33);

  }
  else{
	
  	float q = (A11 + A22 + A33)/3.;
  	float p2 = (A11 - q)*(A11 - q) + (A22 - q)*(A22 - q)
  	  + (A33 - q)*(A33 - q) + 2.f * p1;
  	float p = sqrt(p2 / 6);

  	float B11 =  (1 / p) * (A11 - q);
  	float B22 =  (1 / p) * (A22 - q);
  	float B33 =  (1 / p) * (A33 - q);
  	float B12 =  (1 / p) * A12;
  	float B23 =  (1 / p) * A23;
  	float B13 =  (1 / p) * A13;


  	float phi = 0.f;

  	//  r = 0.5 * det(B)

  	float r  = .5*(B11*(B22*B33-B23*B23)-B12*(B12*B33-B23*B13)
  				   +B13*(B12*B23-B22*B13));   
	
  	if (fabs(r) >1.)
  	  if (r<0)
  		phi = M_PI_F / 3.f;
  	  else
  		phi = 0;
  	else
  	  phi = acos(r) / 3.f;
	
  	eig1 = q + 2*p*cos(phi);
  	eig2 = q + 2*p*cos(phi+2.*M_PI_F/3.);
  	eig3 = q + 2*p*cos(phi+4.*M_PI_F/3.);

	
  }

  // now calculate the eigenvector with resp. to the maximal eigenvalue eig1

  // C = A^2 - (eig2+eig3)*A + eig2*eig3*I

  float esum = (eig2+eig3);
  float eprod = eig2*eig3;
  
  float C11 =  A11*A11 + A12*A12 + A13*A13 - esum*A11 + eprod;
  float C22 =  A12*A12 + A22*A22 + A23*A23 - esum*A22 + eprod;
  float C33 =  A13*A13 + A23*A23 + A33*A33 - esum*A33 + eprod;
  float C12 =  A11*A12 + A12*A22 + A13*A23 - esum*A12;
  float C23 =  A12*A13 + A22*A23 + A23*A33 - esum*A23;
  float C13 =  A11*A13 + A12*A23 + A13*A33 - esum*A13;

  float a,b,c;

  // try first column/row
  a = C11;
  b = C12;
  c = C13;

  if (a*a+b*b+c*c<1.e-6){
	a = C12;
	b = C22;
	c = C23;
  }
  if (a*a+b*b+c*c<1.e-6){
	a = C13;
	b = C23;
	c = C33;
  }
	
  float norm = sqrt(a*a+b*b+c*c);
  a *= 1./norm;
  b *= 1./norm;
  c *= 1./norm;


  output[i0 + Nx*j0 + Nx*Ny*k0 + Nx*Ny*Nz*0] = eig1;
  output[i0 + Nx*j0 + Nx*Ny*k0 + Nx*Ny*Nz*1] = a;
  output[i0 + Nx*j0 + Nx*Ny*k0 + Nx*Ny*Nz*2] = b;
  output[i0 + Nx*j0 + Nx*Ny*k0 + Nx*Ny*Nz*3] = c;

  

}


__kernel void test(global float * output, const int Nx, const int Ny, const int Nz
					){

  

  int i0 = get_global_id(0);
  int j0 = get_global_id(1);
  int k0 = get_global_id(2);


  
  output[i0 + Nx*j0 + Nx*Ny*k0 + Nx*Ny*Nz*0] = i0;
  output[i0 + Nx*j0 + Nx*Ny*k0 + Nx*Ny*Nz*1] = j0;
  output[i0 + Nx*j0 + Nx*Ny*k0 + Nx*Ny*Nz*2] = k0;
  

}
