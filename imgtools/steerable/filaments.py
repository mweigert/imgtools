from numpy import *

if __name__ == '__main__':

    N = 256
    s = 20
    alpha = 8.
    u = zeros((N,N,N),dtype=float32)
    sig = 5.
    t = linspace(-s/2./sig,s/2./sig,s)
    Z,Y,X = meshgrid(t,t,t,indexing = "ij")
    r2 = X**2+Y**2+Z**2

    
    u0 = exp(-alpha*r2)
    x = array([128,128,128])
    vp,vt = 0,0

    for t in range(20000):
        a,b,c = cos(vp)*sin(vt),sin(vp)*sin(vt),cos(vt)
        x = (x+3*array([a,b,c]))%N
        vp += random.normal(0,.3)
        vt += random.normal(0,.3)
        alpha = 8.
        u1 = u0*exp(-(1-alpha)*(a*X+b*Y+c*Z)**2)

        myslice = [slice(max(int(i-s/2),0),min(int(i+s/2),N-1)) for i in x[::-1]]

        try:
            u[myslice] += u1
        except:
            pass
