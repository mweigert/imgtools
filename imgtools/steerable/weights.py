from itertools import product
from scipy.special import binom
from scipy import math

    
    
def S(k,i,j):
    for l in range(k-i+1):
        for m in range(0,i+1):
            if (l+m)==(k-j):
                yield (l,m)

def printWeights(k,i,a_vec=None):
    """for nth order gaussian derivative, a_vec = [1,0...0], len(a_vec) = k+1
    see

    M. Jacob, M. Unser,
"Design of Steerable Filters for Feature Detection Using Canny-Like Criteria,"
IEEE Transactions on Pattern Analysis and Machine Intelligence, vol. 26, no. 8, pp. 1007-1019
    """
    
    if not a_vec:
        a_vec = [1,] + [0]*k
    for j in range(k+1):
        for l,m in S(k,j,i):
            if a_vec[j]!=0:
                print a_vec[j]*binom(k-j,l)*binom(j,m)*(-1)**m, "* cos^%s"%(j+(l-m)),"* sin^%s"%((k-j)-(l-m))



def print_mnp(m,n,p):

    M = m+n+p
    fac = math.factorial
    for i,k,q in product(range(m+1),range(n+1),range(p+1)):
        for j in range(i+1):
            for l in range(k+1):
                s1 = fac(m)*fac(n)*fac(p)*(-1)**(i-j+p-q)
                s2 = fac(m-i)*fac(i-j)*fac(j)*fac(n-k)*fac(k-l)*fac(l)*fac(p-q)*fac(q)
                if M==m-i+n-k+p-q and i-j+k-l==0 and j+l+q==0 and  s2!=0:
                    print m,n,p, "%s cos(theta)^%s cos(phi)^%s sin(theta)%s sin(phi)^%s"%(1.*s1/s2, m-i+j+k-l,m-i+n-k+q,i-j+n-k+l,j+l+p-q)



if __name__ == '__main__':
    print_mnp(2,0,0)
