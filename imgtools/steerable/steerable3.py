#!/usr/bin/env python

"""
Description

Steerable filter implementation for 3d data


see e.g.:

William T. Freeman and Edward H. Adelson: "The Design and Use of Steerable Filters"
IEEE Transactions on Pattern Analysis and Machine Intelligence, 1991,




author: Martin Weigert
email: mweigert@mpi-cbg.de
"""


import numpy as np
from numpy import ones,zeros, float32, sqrt, exp, mean, einsum, arctan2, meshgrid, linspace, cos,sin, argmax, arange, pi, array, amin, amax

from PyOCL import OCLDevice, OCLProcessor

import imgtools

from time import time

from eigen_vals_3x3 import eigen_max_3x3
from itertools import product

def _extended_slice(slice0,N,Nmargin):
    """ returns

    slice1, slice2         where

    slice1   -   slice0 extended by +/- Nmargin
    slice2   -   s.t. data[slice1][slice2] = data[slice0]
    """

    dx1 = Nmargin+min(0,slice0.start-Nmargin)
    dx2 = min(N,slice0.stop+Nmargin) - slice0.stop

    slice1 = slice(slice0.start-dx1,slice0.stop+dx2)
    slice2 = slice(dx1,slice1.stop-slice1.start-dx2)
    return slice1, slice2


class SteerableFilter3():
    """ implements a 3d steerable ridge filter

    ---------

    example usage:


        s = SteerableFilter2()

        # just calculating responses func

        s.run(data, sigma = 4.)


        # calculating maximal responses with angles

        s.run(data, sigma = 4., n_phi = 20, n_theta=20)

    ---------
    afterwards, the following attributes are set:

    s.response_func  -    the response function for any phi/theta
    s.responseMax    -    maximal response to the filter
    s.phiMax         -    angle phi corresponding to maximal response
    s.thetaMax       -    angle theta corresponding to maximal response

    s.outPhiTheta    -    function that gives response for any phi/theta
    """

    def __init__(self,dev=None):
        if dev is None:
            self.dev = imgtools.__DEFAULT_OPENCL_DEVICE__

        if self.dev is None:
            raise ValueError("no OpenCLDevice found...")

    @classmethod
    def normalize(cls,x):
        return (x-amin(x))/(amax(x)-amin(x))

    def _N_from_sigma(self,sigma):
        return int(2.*sigma)*3+1

    def _run_single(self,data,sigma, isMeanFilter = True):
        """ steerable filter to the filter kernel

        f(x) = (1 +.5 x^2) * exp(-x^2/sig^2)


        isMeanFilter = True shifts the filter kernel such that it sums to zero

        """
        N = self._N_from_sigma(sigma)

        x,y,z = linspace(-N/2./sigma,N/2./sigma,N).astype(float32),linspace(-N/2./sigma,N/2./sigma,N).astype(float32),\
                linspace(-N/2./sigma,N/2./sigma,N).astype(float32)

        Z,Y,X = meshgrid(z,y,x, indexing="ij")

        hx0,hy0,hz0 = [1./sqrt(2*pi)*exp(-foo**2/2.) for foo in [x,y,z]]

        t = time()

        filterMean = 1.*mean((1+.5*X**2)*einsum("i,j,k",hx0,hy0,hz0))


        r0 = imgtools.convolve_sep3(data,hx0,hy0,hz0,self.dev)
        rxx = imgtools.convolve_sep3(data,x**2*hx0,hy0,hz0,self.dev)
        ryy = imgtools.convolve_sep3(data,hx0,y**2*hy0,hz0,self.dev)
        rzz = imgtools.convolve_sep3(data,hx0,hy0,z**2*hz0,self.dev)
        rxy = imgtools.convolve_sep3(data,x*hx0,y*hy0,hz0,self.dev)
        ryz = imgtools.convolve_sep3(data,hx0,y*hy0,z*hz0,self.dev)
        rzx = imgtools.convolve_sep3(data,x*hx0,hy0,z*hz0,self.dev)


        rFilterMean = filterMean*imgtools.convolve_sep3(data,ones(N,float32),ones(N,float32),ones(N,float32),self.dev)

        # print "time to construct filter response: %i ms"%(1000.*(time()-t))
        # t = time()

        responses = eigen_max_3x3(rxx,ryy,rzz,rxy,ryz,rzx,self.dev)
        responseMax = r0+.5*responses[0,...] - isMeanFilter*rFilterMean
        self.resp = responses[1:,...]
        phiMax = arctan2(responses[2,...],responses[1,...])
        rho = sqrt(responses[1,...]**2+responses[2,...]**2)
        thetaMax = arctan2(rho,responses[3,...])

        # print filterMean, sigma, sum((1+.5*X**2)*einsum("i,j,k",hx0,hy0,hz0))
        # filterMean = 0

        def _response_func(phi,theta):
            """ returns the response for a given (phi, theta) direction in spherical coordinates"""
            a, b, c = cos(phi)*sin(theta), sin(phi)*sin(theta), cos(theta)
            return r0+.5*(a**2*rxx+b**2*ryy+c**2*rzz+2*a*b*rxy+2*b*c*ryz+2*c*a*rzx) - isMeanFilter*rFilterMean

        self.response_func = _response_func

        # print "time to calculate maximal responses: %i ms"%(1000.*(time()-t))

        return responseMax, phiMax, thetaMax


    def run(self,data,sigma,Ncut=1,isMeanFilter = True):
        if Ncut<=1:
            self.responseMax, self.phiMax, self.thetaMax = self._run_single(data,sigma,isMeanFilter)
        else:
            self.responseMax = empty_like(data,dtype=float32)
            self.phiMax = empty_like(data,dtype=float32)
            self.thetaMax =  empty_like(data,dtype=float32)

            Nz,Ny,Nx = data.shape
            N = self._N_from_sigma(sigma)
            for i0,(i,j,k) in enumerate(product(range(Ncut),repeat=3)):
                print "calculating box  %i/%i"%(i0+1,Ncut**3)
                sx = slice(i*Nx/Ncut,(i+1)*Nx/Ncut)
                sy = slice(j*Ny/Ncut,(j+1)*Ny/Ncut)
                sz = slice(k*Nz/Ncut,(k+1)*Nz/Ncut)
                sx1,sx2 = _extended_slice(sx,Nx,N/2)
                sy1,sy2 = _extended_slice(sy,Ny,N/2)
                sz1,sz2 = _extended_slice(sz,Nz,N/2)

                data_sliced = data[sz1,sy1,sx1].copy()
                _response, _phi, _theta = self._run_single(data_sliced,sigma,isMeanFilter)

                self.responseMax[sz,sy,sx] = _response[sz2,sy2,sx2]
                self.phiMax[sz,sy,sx] = _phi[sz2,sy2,sx2]
                self.thetaMax[sz,sy,sx] = _theta[sz2,sy2,sx2]



def test_pattern3(N=256,s=32):
    u = zeros((N,N,N),dtype=float32)
    sig = 5.
    t = linspace(-s/2./sig,s/2./sig,s)
    Z,Y,X = meshgrid(t,t,t,indexing = "ij")
    r2 = X**2+Y**2+Z**2
    u0 = exp(-r2)

    for i in range(N/s):
        for j in range(N/s):
            for k in range(N/s):
                w = 1.*(N**2/s**2*k+N/s*i+j)/(1.*N**3/s**3)*pi
                t = 1.*k*s/N*pi
                a = cos(w)*sin(t)
                b = sin(w)*sin(t)
                c = cos(t)
                u[s*i:s*i+s,s*j:s*j+s,s*k:s*k+s] = u0 *(1-2*(r2-(a*X+b*Y+c*Z)**2))

    return u

def test_simple():
    from time import time

    t = time()
    data = zeros((256,)*3,float32)
    data += 10

    # data[2,3,3] = .4
    data[30,30,:] += 1.


    s = SteerableFilter3()
    s.run(data,4.,isMeanFilter=False)


    print "size = %s : running time = %2.f ms"%(data.shape,1000.*(time()-t))

if __name__ == '__main__':

    data = zeros((64,)*3,float32)
    data += 10

    # data[2,3,3] = .4
    data[30,30,:] += 1.


    s = SteerableFilter3()
    s.run(data,2.,isMeanFilter=False)
    # s.run(data,2.,True)

    # f = s.response_func

    test_simple()
