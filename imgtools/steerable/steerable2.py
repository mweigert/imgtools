#!/usr/bin/env python

"""
Description

Steerable filter implementation for 2d and 3d data


see e.g.:


M. Jacob, M. Unser,
"Design of Steerable Filters for Feature Detection Using Canny-Like Criteria,"
IEEE Transactions on Pattern Analysis and Machine Intelligence, vol. 26, no. 8, pp. 1007-1019



author: Martin Weigert
email: mweigert@mpi-cbg.de
"""

import logging
logger = logging.getLogger(__name__)
# logger.setLevel(logging.DEBUG)

import numpy as np
from numpy import cos,sin, argmax, arange, pi, array, amin, amax
from PyOCL import OCLDevice, OCLProcessor
import imgtools

# from matplotlib.colors import hsv_to_rgb

from time import time



class SteerableFilter2():
    """ implements 2d steerable filter of 1st and 2nd order derivative of gaussians
    ---------

    example usage:

        data = lena().astype(np.float32)

        s = SteerableFilter2()

        s.run(data, order = 2, sigma = 4., n_angles = 50)

    ---------
    afterwards, the following attributes are set:

    s.responseMax    -    maximal response to gaussian filter
    s.phiMax         -    angle corresponding to maximal response
    s.coloredOrientations    -   rgb image of response/orientation
    s.outPhi         -    function that gives response for any angle
    """


    def __init__(self, dev = None):
        if dev is None:
            self.dev = imgtools.__DEFAULT_OPENCL_DEVICE__

        if self.dev is None:
            raise ValueError("no OpenCLDevice found...")



    @classmethod
    def normalize(cls,x):
        return (x-np.amin(x))/(np.amax(x)-np.amin(x))


    def run(self,data,order,sigma,n_angles):
        N = int(3.*sigma)*2+1

        x,y = np.linspace(-N/2./sigma,N/2./sigma,N),np.linspace(-N/2./sigma,N/2./sigma,N)
        Y,X = np.meshgrid(y,x)

        hx0 = np.exp(-1.*x**2)
        hx0 *= 1./np.sum(hx0)

        hy0 = np.exp(-1.*y**2)
        hy0 *= 1./np.sum(hy0)
        h0 = np.outer(hy0,hx0)


        h0 = np.exp(-1.*(X**2+Y**2))
        h0 *= 1./np.sum(h0)

        # calculate the steering direction responses
        # outPhi(w) returns the response at angle w

        t = time()

        if order==1:
            # hx = 2*X*h0
            # hy = 2*Y*h0
            # outx = convolve2(self.dev,data,hx)
            # outy = convolve2(self.dev,data,hy)
            # #----
            hx_x = hx0
            hx_y = 2.*y*hy0

            hy_x = 2.*x*hx0
            hy_y = hy0
            outx = imgtools.convolve_sep2(data,hx_x,hx_y, self.dev)
            outy = imgtools.convolve_sep2(data,hy_x,hy_y, self.dev)

            outPhi = lambda w: cos(w)*outx +sin(w)*outy

        if order==2:

            # hxx = 2.*(1-2*X**2)*h0
            # hxy = -4.*X*Y*h0
            # hyy = 2.*(1-2*Y**2)*h0
            # #--------

            # the convolutions can be made separable and thus faster...

            hxx_x = hx0
            hxx_y = 2.*(1-2*y**2)*hy0

            hyy_x = 2.*(1-2*x**2)*hx0
            hyy_y = hy0

            hxy_x = -2.*x*hx0
            hxy_y = 2.*y*hy0

            outxx = imgtools.convolve_sep2(data,hxx_x,hxx_y, self.dev)
            outxy = imgtools.convolve_sep2(data,hxy_x,hxy_y, self.dev)
            outyy = imgtools.convolve_sep2(data,hyy_x,hyy_y, self.dev)

            outPhi = lambda w: cos(w)**2*outxx + 2*cos(w)*sin(w)*outxy +sin(w)**2*outyy


        logger.info("time to run: %i ms",1000*(time()-t))

        phis = pi/n_angles*arange(n_angles)
        rots = array([outPhi(phi) for phi in phis])

        phi_max = phis[np.argmax(rots,axis=0)]
        resp_max = np.amax(rots,axis=0)

        # hsv = dstack((self.normalize(phi_max),ones_like(phi_max),self.normalize(resp_max)))
        # color_image = hsv_to_rgb(hsv)
        self.outPhi = outPhi
        self.responseMax = resp_max
        self.phiMax = phi_max
        # self.coloredOrientations = color_image




def test_pattern(N=256,s=10):
    u = zeros((N,N))
    X,Y = np.meshgrid(np.linspace(-1,1,s),np.linspace(-1,1,s))

    for i in range(N/s):
        for j in range(N/s):
            w = 1.*(N/s*i+j)/(1.*N**2/s**2)*pi
            x,y = cos(w)*X+sin(w)*Y,cos(w)*Y-sin(w)*X

            u[s*i:s*i+s,s*j:s*j+s] = np.exp(-2*(10*x**2+y**2))

    return u


def test_lena():
    from imgtools.imgutils.test_images import lena


    data = lena().astype(np.float32)
    # data = test_pattern().astype(np.float32)

    s = SteerableFilter2()

    s.run(data,2,5.,50)


    print amax(s.responseMax)


if __name__ == '__main__':

    from imgtools.imgutils import test_images


    data = test_images.lena().astype(np.float32)
    # data = test_pattern().astype(np.float32)

    s = SteerableFilter2()

    s.run(data,2,5.,50)


    print amax(s.responseMax)
