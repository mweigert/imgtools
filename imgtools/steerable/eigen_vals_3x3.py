
import numpy as np
from PyOCL import OCLDevice, OCLProcessor, cl


def absPath(myPath):
    """ Get absolute path to resource, works for dev and for PyInstaller """
    import sys, os

    try:
        # PyInstaller creates a temp folder and stores path in _MEIPASS
        base_path = sys._MEIPASS
        logger.debug("found MEIPASS: %s "%os.path.join(base_path, os.path.basename(myPath)))

        return os.path.join(base_path, os.path.basename(myPath))
    except Exception:
        base_path = os.path.abspath(os.path.dirname(__file__))
        return os.path.join(base_path, myPath)

    
def eigen_max_3x3(rxx,ryy,rzz,rxy,ryz,rzx, dev):
    """ calculates the maximal eigenvalue and the corresponding eigenvector
    of a symmetric 3x3 matrix

    ( rxx rxy ryz )
    ( rxy ryy rzx )
    ( ryz rzx rzz )

    returns [eig_max, v_x , v_y , v_z]


    just implemented for 3d input arrays yet (i.e. rxx.shape = (Nz,Ny,Nx))
    """

    Nz,Ny,Nx = rxx.shape

    rList = [rxx,ryy,rzz,rxy,ryz,rzx]

    rImgs = [dev.createImage_like(r) for r in rList]

    for rImg,r in zip(rImgs,rList):
        dev.writeImage(rImg,r)

    outBuf = dev.createBuffer(4*Nx*Ny*Nz,dtype=np.float32,
                             mem_flags=cl.mem_flags.READ_WRITE)


    proc = OCLProcessor(dev,absPath("kernels/eigen_vals_3x3.cl"))
    proc.runKernel("eigen_max",(Nx,Ny,Nz),None,outBuf,
                   np.int32(Nx),np.int32(Ny),np.int32(Nz),*rImgs)

    out = dev.readBuffer(outBuf,dtype=np.float32).reshape((4,Nz,Ny,Nx))

    return out



if __name__ == '__main__':
    N = 100

    dev= OCLDevice(useDevice=1)


    # r0 = [rxx,ryy,rzz,rxy,ryz,rzx]
    r0 = [1.,2.,3.,4,5,6]
    r0 = np.random.randint(1,10,6)
    r0 = [0]*6
    # r0 = [1.,2.,3.,0,0,0]

    # reference
    A = array([[r0[0],r0[3],r0[5]],[r0[3],r0[1],r0[4]],[r0[5],r0[4],r0[2]]])

    e,v = np.linalg.eig(A)
    ind = np.argsort(e,axis=-1)

    e = e[ind]
    v = v[:,ind]
    # print A
    # print e
    # print dot(A,A)-e[0]*A-e[1]*A+e[1]*e[0]*identity(3)
    print v[:,-1]

    Nz,Ny,Nx = (N,)*3

    rList = [r*np.ones((Nz,Ny,Nx),dtype=np.float32) for r in r0]


    out = eigen_max_3x3(dev,*rList)

    print out[:,0,0,0]
