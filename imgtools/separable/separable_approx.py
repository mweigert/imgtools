
from numpy import *

from scipy import linalg
from sktensor import dtensor, cp_als
from time import time


def separable_series2(h,N=1):
    """ finds separable approximations to the 2d function 2d h

    returns res = (hx, hy)[N]
    s.t. h \approx sum_i outer(res[i,0],res[i,1])
    """
    U,S,V = linalg.svd(h)

    hx = [-U[:,n]*sqrt(S[n]) for n in range(N)]
    hy = [-V[n,:]*sqrt(S[n]) for n in range(N)]
    return array(zip(hx,hy))

def separable_approx2(h,N=1):
    """ returns the N first approximations to the 2d function h
    whose sum should be h
    """
    return cumsum([outer(fy,fx) for fy,fx in separable_series2(h,N)],0)


def __separable_series3(h,N=1):
    """ finds separable approximations to the 3d kernel h
    returns res = (hx,hy,hz)[N]
    s.t. h \approx sum_i outer(res[i,0],res[i,1],res[i,2])
    FIXME: This is just a naive and slow first try!
    """
    hx,hy,hz = [],[],[]
    u = h.copy()
    P, fit, itr, exectimes = cp_als(dtensor(u), N)
    hx,hy,hz = [[(P.lmbda[n])**(1./3)*array(P.U[i])[:,n] for n in range(N)] for i in range(3)]
    print "lambdas= %s \nfit = %s \niterations= %s "%(P.lmbda, fit, itr)
    return array(zip(hx,hy,hz))

def _splitrank3(h):
    P, fit, itr, exectimes = cp_als(dtensor(h.copy()), 1)
    hx,hy,hz = [(P.lmbda[0])**(1./3)*array(P.U[i])[:,0] for i in range(3)]
    print "lambdas= %s \nfit = %s \niterations= %s "%(P.lmbda, fit, itr)
    return hx,hy,hz, P.toarray()

def separable_series3(h,N=1):
    """ finds separable approximations to the 3d kernel h
    returns res = (hx,hy,hz)[N]
    s.t. h \approx sum_i einsum("i,j,k",res[i,0],res[i,1],res[i,2])

    FIXME: This is just a naive and slow first try!
    """

    
    hx,hy,hz = [],[],[]
    res = h.copy()
    for i in range(N):
        _hx,_hy,_hz, P  = _splitrank3(res)
        res -= P
        hx.append(_hx)
        hy.append(_hy)
        hz.append(_hz)
    return array(zip(hx,hy,hz))

def separable_approx3(h,N=1):
    """ returns the N first approximations to the 3d function h
    """
    return cumsum([einsum("i,j,k",fz,fy,fx) for fz,fy,fx in separable_series3(h,N)],0)

if __name__ == '__main__':


    x = linspace(-1,1,100)

    u = einsum("i,j,k",x,sin(4*pi*x),exp(-10*x**2))


    sep = separable_series3(u,1)
