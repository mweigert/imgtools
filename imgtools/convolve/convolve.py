import logging
logger = logging.getLogger(__name__)

import os
import numpy as np

from PyOCL import OCLDevice, OCLProcessor, cl

import imgtools

import sys

def absPath(myPath):
    """ Get absolute path to resource, works for dev and for PyInstaller """
    import sys, os

    try:
        # PyInstaller creates a temp folder and stores path in _MEIPASS
        base_path = sys._MEIPASS
        logger.debug("found MEIPASS: %s "%os.path.join(base_path, os.path.basename(myPath)))

        return os.path.join(base_path, os.path.basename(myPath))
    except Exception:
        base_path = os.path.abspath(os.path.dirname(__file__))
        return os.path.join(base_path, myPath)



__all__ = ['fftconvolve','convolve2','convolve3','convolve_sep2','convolve_sep3']


def fftconvolve(data,h):
    """ a simple reimplementation of scipy.signal.fftconvolve
    as the latter is dead slow
    """

    s1 = np.array(data.shape)
    s2 = np.array(h.shape)

    size = s1 + s2 - 1

    # Always use 2**n-sized FFT
    # fSize = (2 ** np.ceil(np.log2(size))).astype(np.uint)
    fSize = size.astype(np.int)

    d_f = np.fft.rfftn(data,fSize)
    h_f = np.fft.rfftn(h,s=fSize)

    fslice = tuple(slice(s_2/2,s_1+s_2/2) for s_1,s_2 in zip(s1,s2))
    return np.fft.irfftn(d_f*h_f)[fslice]



def convolve2(data,h, dev = None):
    """convolves 2d data with kernel h on the GPU Device dev
    boundary conditions are clamping to edge.
    h is converted to float32

    if dev == None a new one is created
    """

    if dev is None:
        dev = imgtools.__DEFAULT_OPENCL_DEVICE__

    if dev is None:
        raise ValueError("no OpenCLDevice found...")

    dtype = data.dtype.type

    dtypes_kernels = {np.float32:"convolve2d_float",
                      np.uint16:"convolve2d_short"}

    if not dtype in dtypes_kernels.keys():
        raise TypeError("data type %s not supported yet, please convert to:"%dtype,dtypes_kernels.keys())


    proc = OCLProcessor(dev,absPath("kernels/convolve_kernels.cl"))

    Ny,Nx = h.shape

    hbuf = dev.createBuffer(Nx*Ny,dtype=np.float32,mem_flags= cl.mem_flags.READ_ONLY)
    inImg = dev.createImage_like(data)
    outImg = dev.createImage_like(data,mem_flags="READ_WRITE")

    dev.writeImage(inImg,data)

    dev.writeBuffer(hbuf,h.astype(np.float32).flatten())


    proc.runKernel(dtypes_kernels[dtype],inImg.shape,None,inImg,hbuf,np.int32(Nx),np.int32(Ny),outImg)


    return dev.readImage(outImg)


def convolve3(data,h, dev = None):
    """convolves 3d data with kernel h on the GPU Device dev
    boundary conditions are clamping to edge.
    h is converted to float32

    if dev == None a new one is created

    """

    if dev is None:
        dev = imgtools.__DEFAULT_OPENCL_DEVICE__

    if dev is None:
        raise ValueError("no OpenCLDevice found...")

    dtype = data.dtype.type

    dtypes_kernels = {np.float32:"convolve3d_float_buf",
                      np.uint16:"convolve3d_short_buf"}

    if not dtype in dtypes_kernels.keys():
        raise TypeError("data type %s not supported yet, please convert to:"%dtype,dtypes_kernels.keys())

    proc = OCLProcessor(dev,absPath("kernels/convolve_kernels.cl"))

    Nz,Ny,Nx = h.shape
    hbuf = dev.createBuffer(Nx*Ny*Nz,dtype=np.float32)

    inImg = dev.createImage_like(data)
    # create buffer as write to 3d images is not always supported...

    outBuf = dev.createBuffer(np.prod(data.shape),dtype=dtype,mem_flags= cl.mem_flags.READ_WRITE)
    dev.writeImage(inImg,data)

    dev.writeBuffer(hbuf,h.astype(np.float32).flatten())

    proc.runKernel(dtypes_kernels[dtype],inImg.shape,None,inImg,hbuf,np.int32(Nx),np.int32(Ny),np.int32(Nz),
                   np.int32(data.shape[2]),np.int32(data.shape[1]),outBuf)


    return dev.readBuffer(outBuf,dtype=dtype).reshape(data.shape)

def _convolve3_img(data,h, dev = None):
    """convolves 3d data with kernel h on the GPU Device dev
    boundary conditions are clamping to edge.
    h is converted to float32

    if dev == None a new one is created

    """

    if dev is None:
        dev = imgtools.__DEFAULT_OPENCL_DEVICE__

    if dev is None:
        raise ValueError("no OpenCLDevice found...")

    dtype = data.dtype.type

    dtypes_kernels = {np.float32:"convolve3d_float",
                      np.uint16:"convolve3d_short"}

    if not dtype in dtypes_kernels.keys():
        raise TypeError("data type %s not supported yet, please convert to:"%dtype,dtypes_kernels.keys())

    proc = OCLProcessor(dev,absPath("kernels/convolve_kernels.cl"))

    Nz,Ny,Nx = h.shape
    hbuf = dev.createBuffer(Nx*Ny*Nz,dtype=np.float32)

    inImg = dev.createImage_like(data)
    outImg = dev.createImage_like(data,mem_flags="READ_WRITE")


    dev.writeImage(inImg,data)

    dev.writeBuffer(hbuf,h.astype(np.float32).flatten())

    proc.runKernel(dtypes_kernels[dtype],inImg.shape,None,inImg,hbuf,np.int32(Nx),np.int32(Ny),np.int32(Nz),outImg)


    return dev.readImage(outImg)

def convolve_sep2(data,hx,hy, dev = None):
    """convolves 2d data with kernel h = outer(hx,hy) on the GPU Device dev
    boundary conditions are clamping to edge.
    hx, hy are  converted to float32

    if dev == None a new one is created

    """

    if dev is None:
        dev = imgtools.__DEFAULT_OPENCL_DEVICE__

    if dev is None:
        raise ValueError("no OpenCLDevice found...")

    dtype = data.dtype.type

    dtypes_kernels = {np.float32:"convolve_sep2d_float",
                      np.uint16:"convolve_sep2d_short"}

    if not dtype in dtypes_kernels.keys():
        raise TypeError("data type %s not supported yet, please convert to:"%dtype,dtypes_kernels.keys())


    proc = OCLProcessor(dev,absPath("kernels/convolve_kernels.cl"))

    Ny,Nx = len(hy),len(hx)

    hxbuf = dev.createBuffer(Nx,dtype=np.float32,mem_flags= cl.mem_flags.READ_ONLY)
    hybuf = dev.createBuffer(Ny,dtype=np.float32,mem_flags= cl.mem_flags.READ_ONLY)


    inImg = dev.createImage_like(data)
    outImg = dev.createImage_like(data,mem_flags="READ_WRITE")
    tmpImg = dev.createImage_like(data,mem_flags="READ_WRITE")

    dev.writeImage(inImg,data)

    dev.writeBuffer(hxbuf,hx.astype(np.float32).flatten())
    dev.writeBuffer(hybuf,hy.astype(np.float32).flatten())


    proc.runKernel(dtypes_kernels[dtype],inImg.shape,None,inImg,hxbuf,np.int32(Nx),tmpImg,np.int32(1))
    proc.runKernel(dtypes_kernels[dtype],inImg.shape,None,tmpImg,hybuf,np.int32(Ny),outImg,np.int32(2))


    return dev.readImage(outImg)

def convolve_sep3(data,hx,hy,hz, dev = None, logTime = False):
    """convolves 3d data with kernel h = outer(hx,hy,hz) on the GPU Device dev
    boundary conditions are clamping to edge.
    hx, hy, hz are converted to float32

    if dev == None a new one is created

    """

    if dev is None:
        dev = imgtools.__DEFAULT_OPENCL_DEVICE__

    if dev is None:
        raise ValueError("no OpenCLDevice found...")

    if logTime:
        from time import time
        tic = time()

    dtype = data.dtype.type

    dtypes_kernels = {np.float32:"convolve_sep3d_float",
                      np.uint16:"convolve_sep3d_short"}

    if not dtype in dtypes_kernels.keys():
        print "data type %s not supported yet, please convert to:"%dtype,dtypes_kernels.keys()
        print "converting to float32..."
        data = data.astype(np.float32)
        dtype = data.dtype.type
        # raise TypeError("data type %s not supported yet, please convert to:"%dtype,dtypes_kernels.keys())


    proc = OCLProcessor(dev,absPath("kernels/convolve_kernels.cl"))

    Nz, Ny,Nx = len(hz),len(hy),len(hx)

    hxbuf = dev.createBuffer(Nx,dtype=np.float32,mem_flags= cl.mem_flags.READ_ONLY)
    hybuf = dev.createBuffer(Ny,dtype=np.float32,mem_flags= cl.mem_flags.READ_ONLY)
    hzbuf = dev.createBuffer(Nz,dtype=np.float32,mem_flags= cl.mem_flags.READ_ONLY)

    inImg = dev.createImage_like(data)
    outImg = dev.createImage_like(data,mem_flags="READ_WRITE")
    tmpImg = dev.createImage_like(data,mem_flags="READ_WRITE")

    dev.writeImage(inImg,data)


    dev.writeBuffer(hxbuf,hx.astype(np.float32).flatten())
    dev.writeBuffer(hybuf,hy.astype(np.float32).flatten())
    dev.writeBuffer(hzbuf,hz.astype(np.float32).flatten())

    if logTime:
        foo = dev.readBuffer(hxbuf)
        print "time to compile and push data to gpu %.2f"%(1000.*(time()-tic))
        tic = time()



    proc.runKernel(dtypes_kernels[dtype],inImg.shape,None,inImg,hxbuf,np.int32(Nx),outImg,np.int32(1))
    proc.runKernel(dtypes_kernels[dtype],inImg.shape,None,outImg,hybuf,np.int32(Ny),tmpImg,np.int32(2))
    proc.runKernel(dtypes_kernels[dtype],inImg.shape,None,tmpImg,hzbuf,np.int32(Nz),outImg,np.int32(4))

    if logTime:
        dev.readImage(outImg)
        print "time to convolve %.2f"%(1000*(time()-tic))


    return dev.readImage(outImg)



def test_convolve2():
    data  = 10*np.ones((100,100))
    h = np.ones((30,30))

    #float
    out = convolve2(data.astype(np.float32),h)
    np.testing.assert_equal(out[40,40],data[40,40]*np.sum(h))

    #short
    out = convolve2(data.astype(np.uint16),h)
    np.testing.assert_equal(out[40,40],data[40,40]*np.sum(h))


def test_convolve_sep2():
    data  = 10*np.ones((100,100))
    hx = np.ones(10)
    hy = 2.*np.ones(10)

    #float
    out = convolve_sep2(data.astype(np.float32),hx,hy)
    np.testing.assert_equal(out[40,40],data[40,40]*np.sum(hx)*np.sum(hy))

    #short
    out = convolve_sep2(data.astype(np.uint16),hx,hy)
    np.testing.assert_equal(out[40,40],data[40,40]*np.sum(hx)*np.sum(hy))



def test_convolve3():
    from time import time

    data  = 10*np.ones((64,)*3,dtype=np.float32)
    h = np.ones((10,10,10))

    sumTime = 0
    for dtype in (np.float32,np.uint16):
        t = time()
        out = convolve3(data.astype(dtype),h)
        sumTime += time()-t
        np.testing.assert_equal(out[30,30,30],data[30,30,30]*np.sum(h))

    print "convolving %s image with %s kernel in %.2f ms"%(data.shape,h.shape,1000*sumTime/2)


def test_convolve_sep3():
    from time import time

    data  = 2*np.ones((64,)*3,dtype=np.float32)
    hx = np.ones(20)
    hy = 1.*np.ones(20)
    hz = 1.*np.ones(20)

    sumTime = 0
    for dtype in (np.float32,np.uint16):
        t = time()
        out = convolve_sep3(data.astype(dtype),hx,hy,hz)
        sumTime += time()-t
        np.testing.assert_equal(out[30,30,30],data[30,30,30]*np.sum(hx)*np.sum(hy)*np.sum(hz))

    print "convolving %s image with %s kernel (separable) in %.2f ms"%(data.shape,(len(hx),len(hy),len(hz)),1000*sumTime/2)


def test_sizes():
    from time import time

    data  = 10*np.ones((32,)*3,dtype=np.float32)

    for N in range(25,40,2):
        h = np.ones((N,)*3)
        print "convolving with kernel of size %s " % (h.shape,)
        out = convolve3(data,h)

    h = np.ones((20,)*3)
    for N in range(60,100,10):
        data  = np.ones((N,)*3,dtype=np.float32)
        print "convolving image of size %s " % (data.shape,)
        out = convolve3(data,h)


def test_fftconvolve():
    from scipy.misc import lena
    x = np.linspace(-1,1,100)
    X,Y = np.meshgrid(x,x)

    h = np.exp(-100*(X**2+Y**2))

    return fftconvolve(lena(),h)


def test_time__sep3():

    Ns = np.arange(200,300,50)
    Nhs = np.arange(5,64,10)

    for N in Ns:
        data = np.ones((N,)*3,np.float32)
        for Nh in Nhs:
            print "+++++    ", N,Nh
            hx = np.ones(Nh)
            hy = np.ones(Nh)
            hz = np.ones(Nh)
            convolve_sep3(data,hx,hy,hz)


if __name__ == '__main__':

    # test_fftconvolve()

    # test_convolve2()
    # test_convolve_sep2()
    # test_convolve3()


    # test_convolve_sep3()
    # test_sizes()


    test_time__sep3()
