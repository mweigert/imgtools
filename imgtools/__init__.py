
"""
Description

imgtools: some algorithms for image processing accelerated by OpenCL

available functions:


convolutions:

   from imgtools.convolve import convolve_sep3, fftconvolve

   result1 = convolve_sep3(data,hx,hy,hz)
   result2 = fftconvolve(data,einsum("i,j,k",hx,hy,hz))   #should be the same


steerable filters:

   e.g.

   s = SteerableFilter3()

   s.run(data, sigma = 4.)




author: Martin Weigert
email: mweigert@mpi-cbg.de
"""


import os
__CONFIGFILE__ = os.path.expanduser("~/.imgtools")



global __DEFAULT_OPENCL_DEVICE__
__DEFAULT_OPENCL_DEVICE__ = None


global __OPENCLDEVICE__
__OPENCLDEVICE__ = None


import ConfigParser, StringIO

class MyConfigParser(ConfigParser.ConfigParser):
    def __init__(self,fName = None):
        ConfigParser.ConfigParser.__init__(self)
        self.dummySection = "DUMMY"
        if fName:
            self.read(fName)


    def read(self, fName):
        try:
            text = open(fName).read()
        except IOError:
            raise IOError()
        else:
            file = StringIO.StringIO("[%s]\n%s"%(self.dummySection,text))
            self.readfp(file, fName)

    def get(self,varStr):
        return ConfigParser.ConfigParser.get(self,self.dummySection,varStr)


if not __OPENCLDEVICE__:
    try:
        __imgtools_config_parser = MyConfigParser(__CONFIGFILE__)
        __OPENCLDEVICE__ = int(__imgtools_config_parser.get("OPENCLDEVICE"))
    except:
        __OPENCLDEVICE__ = 0

import PyOCL


if not __DEFAULT_OPENCL_DEVICE__:
    try:
        __DEFAULT_OPENCL_DEVICE__ = PyOCL.OCLDevice(useDevice = __OPENCLDEVICE__)
    except:
        __DEFAULT_OPENCL_DEVICE__ = None



def setDefaultOpenCLDevice(dev):
    __DEFAULT_OPENCL_DEVICE__ = dev

def setOpenCLDevice(num):
    __OPENCLDEVICE__ = num




import logging
logging.basicConfig(format='%(levelname)s:%(name)s | %(message)s')
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
#logger.setLevel(logging.DEBUG)


from imgtools.convolve import *

from imgtools.imgutils import *

from imgtools.denoise import *


# from steerable.steerable2 import *
from imgtools.steerable import *

from imgtools.separable import *


from imgtools.ocl_fft import *

from imgtools.algos import *
