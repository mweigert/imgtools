# #!/usr/bin/env python

# """
# simple data models

# generic containers are defined for BScope Spim Data (SpimData)
# and Tiff files (TiffData).
# Extend it if you want to and change the DataLoadModel.chooseContainer to
# accept it via dropg

# author: Martin Weigert
# email: mweigert@mpi-cbg.de
# """

# import logging
# logger = logging.getLogger(__name__)

# import os
# import numpy as np
# import time
# from threading import Thread
# import re
# from collections import defaultdict

# from imgtools.imgutils.spimutils import fromSpimFolder, parseMetaFile, parseIndexFile

# def absPath(myPath):
#     """ Get absolute path to resource, works for dev and for PyInstaller """
#     import sys

#     try:
#         # PyInstaller creates a temp folder and stores path in _MEIPASS
#         base_path = sys._MEIPASS
#         logger.debug("found MEIPASS: %s "%os.path.join(base_path, os.path.basename(myPath)))

#         return os.path.join(base_path, os.path.basename(myPath))
#     except Exception:
#         base_path = os.path.abspath(os.path.dirname(__file__))
#         return os.path.join(base_path, myPath)


# ############################################################################
# """
# The next classes define simple 4d Data Structures that implement the interface
# given by GenericData
# """

# class GenericData():
#     """abstract base class for 4d data

#     if you wanna sublass it, just overwrite self.size() and self.__getitem__()

#     """
#     dataFileError = Exception("not a valid file")
#     def __init__(self, name = ""):
#         self.stackSize = None
#         self.stackUnits = None
#         self.name = name

#     # def setStackSize(self, stackSize):
#     #     self.stackSize  = list(stackSize)

#     def sizeT(self):
#         return self.size()[0]

#     def size(self):
#         return self.stackSize

#     def __getitem__(self,i):
#         return None


# class SpimData(GenericData):
#     """data class for spim data saved in folder fName
#     fname/
#     |-- metadata.txt
#     |-- data/
#        |--data.bin
#        |--index.txt
#     """
#     def __init__(self,fName = ""):
#         GenericData.__init__(self, fName)
#         self.load(fName)

#     def load(self,fName):
#         if fName:
#             try:
#                 self.stackSize = parseIndexFile(os.path.join(fName,"data/index.txt"))
#                 self.stackUnits = parseMetaFile(os.path.join(fName,"metadata.txt"))
#                 self.fName = fName
#             except Exception as e:
#                 print e
#                 self.fName = ""
#                 raise Exception("couldnt open %s as SpimData"%fName)

#             try:
#                 # try to figure out the dimension of the dark frame stack
#                 darkSizeZ = os.path.getsize(os.path.join(self.fName,"data/darkstack.bin"))/2/self.stackSize[2]/self.stackSize[3]
#                 with open(os.path.join(self.fName,"data/darkstack.bin"),"rb") as f:
#                     self.darkStack = np.fromfile(f,dtype="<u2").reshape([darkSizeZ,self.stackSize[2],self.stackSize[3]])

#             except Exception as e:
#                 logger.warning("couldn't find darkstack (%s)",e)


#     def __getitem__(self,pos):
#         if self.stackSize and self.fName:
#             if pos<0 or pos>=self.stackSize[0]:
#                 raise IndexError("0 <= pos <= %i, but pos = %i"%(self.stackSize[0]-1,pos))


#             pos = max(0,min(pos,self.stackSize[0]-1))
#             voxels = np.prod(self.stackSize[1:])
#             # use int64 for bigger files
#             offset = np.int64(2)*pos*voxels

#             with open(os.path.join(self.fName,"data/data.bin"),"rb") as f:
#                 f.seek(offset)
#                 return np.fromfile(f,dtype="<u2",
#                 count=voxels).reshape(self.stackSize[1:])
#         else:
#             return None




# ############################################################################
# """
# this defines the qt enabled data models based on the GenericData structure

# each dataModel starts a prefetching thread, that loads next timepoints in
# the background
# """


# class DataLoadThread(Thread):
#     """the prefetching thread for each data model"""
#     def __init__(self, _rwLock, nset = set(), data = None,dataContainer = None):
#         Thread.__init__(self)
#         self._rwLock = _rwLock
#         if nset and data and dataContainer:
#             self.load(nset, data, dataContainer)


#     def load(self, nset, data, dataContainer):
#         self.nset = nset
#         self.data = data
#         self.dataContainer = dataContainer


#     def run(self):
#         self.stopped = False
#         while not self.stopped:
#             kset = set(self.data.keys())
#             dkset = kset.difference(set(self.nset))
#             dnset = set(self.nset).difference(kset)

#             for k in dkset:
#                 del(self.data[k])

#             if dnset:
#                 logger.debug("preloading %s", list(dnset))
#                 for k in dnset:
#                     newdata = self.dataContainer[k]
#                     self._rwLock.lockForWrite()
#                     self.data[k] = newdata
#                     self._rwLock.unlock()
#                     logger.debug("preload: %s",k)
#                     time.sleep(.0001)

#             time.sleep(.0001)


# class DataModel(Object):
#     """the data model
#     """
#     _rwLock = QtCore.QReadWriteLock()

#     def __init__(self, dataContainer = None, prefetchSize = 0):
#         super(DataModel,self).__init__()
#         self.dataLoadThread = DataLoadThread(self._rwLock)
#         self._dataSourceChanged.connect(self.dataSourceChanged)
#         self._dataPosChanged.connect(self.dataPosChanged)
#         if dataContainer:
#             self.setContainer(dataContainer, prefetchSize)

#     @classmethod
#     def fromPath(self,fName, prefetchSize = 0):
#         d = DataModel()
#         d.loadFromPath(fName,prefetchSize)
#         return d

#     def setContainer(self,dataContainer = None, prefetchSize = 0):
#         self.dataContainer = dataContainer
#         self.prefetchSize = prefetchSize
#         self.nset = []
#         self.data = defaultdict(lambda: None)

#         if self.dataContainer:
#             if prefetchSize > 0:
#                 self.stopDataLoadThread()
#                 self.dataLoadThread.load(self.nset,self.data, self.dataContainer)
#                 self.dataLoadThread.start(priority=QtCore.QThread.LowPriority)
#             self._dataSourceChanged.emit()
#             self.setPos(0)


#     def __repr__(self):
#         return "DataModel: %s \t %s"%(self.dataContainer.name,self.size())

#     def dataSourceChanged(self):
#         logger.info("data source changed:\n%s",self)

#     def dataPosChanged(self, pos):
#         logger.info("data position changed to %i",pos)



#     def stopDataLoadThread(self):
#         self.dataLoadThread.stopped = True

#     def prefetch(self,pos):
#         self._rwLock.lockForWrite()
#         self.nset[:] = self.neighborhood(pos)
#         self._rwLock.unlock()

#     def sizeT(self):
#         if self.dataContainer:
#             return self.dataContainer.sizeT()

#     def size(self):
#         if self.dataContainer:
#             return self.dataContainer.size()

#     def name(self):
#         if self.dataContainer:
#             return self.dataContainer.name

#     def stackUnits(self):
#         if self.dataContainer:
#             return self.dataContainer.stackUnits

#     def setPos(self,pos):
#         if pos<0 or pos>=self.sizeT():
#             raise IndexError("setPos(pos): %i outside of [0,%i]!"%(pos,self.sizeT()-1))
#             return

#         if not hasattr(self,"pos") or self.pos != pos:
#             self.pos = pos
#             self._dataPosChanged.emit(pos)
#             self.prefetch(self.pos)


#     def __getitem__(self,pos):
#         # self._rwLock.lockForRead()
#         if not hasattr(self,"data"):
#             return None

#         if not self.data.has_key(pos):
#             newdata = self.dataContainer[pos]
#             self._rwLock.lockForWrite()
#             self.data[pos] = newdata
#             self._rwLock.unlock()



#         if self.prefetchSize > 0:
#             self.prefetch(pos)

#         return self.data[pos]



#     def neighborhood(self,pos):
#         # FIXME mod stackSize!
#         return np.arange(pos,pos+self.prefetchSize+1)%self.sizeT()

#     def loadFromPath(self,fName, prefetchSize = 0):
#         if re.match(".*\.tif",fName):
#             self.setContainer(TiffData(fName),prefetchSize = 0)
#         elif re.match(".*\.(png|jpg|bmp)",fName):
#             self.setContainer(Img2dData(fName),prefetchSize = 0)
#         else:
#             self.setContainer(SpimData(fName),prefetchSize)




# def test_spimdata():
#     d = SpimData("/Users/mweigert/Data/HisGFP")

#     m = DataModel(d)
#     print m
#     for pos in range(m.sizeT()):
#         print pos
#         print np.mean(m[pos])


# def test_tiffdata():
#     d = TiffData("/Users/mweigert/Data/droso_test.tif")

#     m = DataModel(d)
#     print m
#     for pos in range(m.sizeT()):
#         print pos
#         print np.mean(m[pos])


# def test_numpydata():
#     d = NumpyData(np.ones((10,100,100,100)))


#     m = DataModel(d)

#     print m
#     for pos in range(m.sizeT()):
#         print pos
#         print np.mean(m[pos])

# def test_frompath():
#     m = DataModel.fromPath("/Users/mweigert/Data/HisGFP")
#     m = DataModel.fromPath("/Users/mweigert/Data/droso_test.tif")


# def test_speed():
#     import time

#     fName  = "/Users/mweigert/Data/Drosophila_full"

#     t = []
#     d = DataModel.fromPath(fName,1)

#     for i in range(100):
#         print i

#         if i%10==0:
#             a = d[i/10]


#         time.sleep(.1)
#         t.append(time.time())


# if __name__ == '__main__':

#     # test_spimdata()

#     # test_tiffdata()
#     # test_numpydata()

#     # test_speed()

#     # test_frompath()


#     # d = Img2dData("/Users/mweigert/Data/test_images/actin.jpg")

#     m = DataModel.fromPath("/Users/mweigert/Data/test_images/actin.jpg")


#     print m.size()


#     print m[0].shape
