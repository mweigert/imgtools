import numpy as np

def pad_to_shape(h,dshape, mode = "constant"):
    if h.shape == dshape:
        return h

    diff = np.array(dshape)- np.array(h.shape)
    #first shrink
    slices  = [slice(-x/2,x/2) if x<0 else slice(None,None) for x in diff]
    res = h[slices]
    #then padd
    return np.pad(res,[(d/2,d-d/2) if d>0 else (0,0) for d in diff],mode=mode)



def ZYX(N = 128, x1 = -1., x2 = 1.):
    x = np.linspace(x1,x2,N)
    Z,Y,X = np.meshgrid(x,x,x,indexing="ij")
    return Z,Y,X
