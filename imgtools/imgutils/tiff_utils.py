import numpy as np
from PIL import Image

try:
    from libtiff import TIFFfile, TIFFimage
except ImportError:
    print "could not import libtiff"



def read3dTiff(fName):
    img = Image.open(fName)
    i = 0
    data = []
    while True:
        try:
            img.seek(i)
        except EOFError:
            break
        data.append(np.asarray(img))
        i += 1

    return np.array(data)


def read3dTiff_libtiff(fName):
    """ still has problems with matlab created tif"""
    tif = TIFFfile(fName)
    data = tif.get_samples()[0][0]
    return data

def getTiffSize(fName):
    img = Image.open(fName, 'r')
    depth = 0
    while True:
        try:
            img.seek(depth)
        except Exception as e:
            break
        depth += 1

    return (depth,)+img.size[::-1]

def write3dTiff(data,fName):
    tiff = TIFFimage(data, description='')
    tiff.write_file(fName, compression='none')



if __name__ == '__main__':

    pass
    # fName = "/Users/mweigert/Data/droso_test.tif"
    # d = read3dTiff(fName)
    # write3dTiff(d,"foo.tif")
