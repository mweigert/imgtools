


import os
import numpy as np
import re
import itertools
from PIL import Image
from tiff_utils import read3dTiff, getTiffSize


def absPath(s):
    return os.path.join(os.path.dirname(__file__),s)


def parseIndexFile(fname):
    """
    returns (t,z,y,z) dimensions of a spim stack
    """
    try:
        lines = open(fname).readlines()
    except IOError:
        print "could not open and read ",fname
        return None

    items = lines[0].replace("\t",",").split(",")
    try:
        stackSize = [int(i) for i in items[-4:-1]] +[len(lines)]
    except Exception as e:
        print e
        print "couldnt parse ", fname
        return None
    stackSize.reverse()
    return stackSize


def parseMetaFile(fName):
    """
    returns pixelSizes (dx,dy,dz)
    """

    with open(fName) as f:
        s = f.read()
        try:
            z1 = float(re.findall("StartZ.*",s)[0].split("\t")[2])
            z2 = float(re.findall("StopZ.*",s)[0].split("\t")[2])
            zN = float(re.findall("NumberOfPlanes.*",s)[0].split("\t")[2])

            return (.162,.162, (1.*z2-z1)/zN)
        except Exception as e:
            print e
            print "coulndt parse ", fName
            return (1.,1.,1.)


# def fromSpimFolder(fName,dataFileName="data/data.bin",indexFileName="data/index.txt",count=-1):
#     stackSize = parseIndexFile(os.path.join(fName,indexFileName))
#     if stackSize:
#         if count>0:
#             stackSize[0] = min(count,stackSize[0])
#         print stackSize

#         return np.fromfile(os.path.join(fName,dataFileName),dtype="<u2",count=np.prod(stackSize)).reshape(stackSize)


def fromSpimFolder(fName,dataFileName="data/data.bin",indexFileName="data/index.txt",pos=0,count=1):
    stackSize = parseIndexFile(os.path.join(fName,indexFileName))

    if stackSize:
        # clamp to pos to stackSize
        pos = min(pos,stackSize[0]-1)
        pos = max(pos,0)

        if count>0:
            stackSize[0] = min(count,stackSize[0]-pos)
        else:
            stackSize[0] = max(0,stackSize[0]-pos)

        with open(os.path.join(fName,dataFileName),"rb") as f:
            f.seek(2*pos*np.prod(stackSize[1:]))
            return np.fromfile(f,dtype="<u2",
                               count=np.prod(stackSize)).reshape(stackSize)


def fromSpimFile(fName,stackSize):
    return np.fromfile(fName,dtype="<u2").reshape(stackSize)


def saveAsSpimFolder(dirName,data,stackUnits = (1.,1.,1.), force = True):
    if not os.path.exists(dirName):
        os.makedirs(dirName)
    else:
        if not force:
            print dirname + " already exists and force = False, skipping..."
            return



def createSpimFolder(fName,data = None, stackSize= [10,10,32,32], stackUnits = (1,1,1)):
    if not os.path.exists(fName):
        os.makedirs(fName)
    if not os.path.exists(os.path.join(fName,"data")):
        os.makedirs(os.path.join(fName,"data"))



    if data != None:
        stackSize = data.shape
        datafName = os.path.join(fName,"data/data.bin")
        with open(datafName,"wa") as f:
                data.astype(np.uint16).tofile(f)

    Nt,Nz,Ny,Nx = stackSize

    indexfName = os.path.join(fName,"data/index.txt")
    with open(indexfName,"w") as f:
        for i in range(Nt):
            f.write("%i\t0.0000\t1,%i,%i,%i\t0\n"%(i,Nx,Ny,Nz))

    metafName = os.path.join(fName,"metadata.txt")
    with open(metafName,"w") as f:
        f.write("timelapse.NumberOfPlanes\t=\t%i\t0\n"%Nz)
        f.write("timelapse.StartZ\t=\t0\t0\n")
        f.write("timelapse.StopZ\t=\t%.2f\t0\n"%(stackUnits[2]*Nz))




def openImageFile( fName):
    with  open(fName) as f:
        try:
            from PIL import Image
            data = np.asarray(Image.open(f).convert('L'))

        except ImportError:
            print "no PIL found, trying imread..."
        except:
            print "couldnt open with PIL"
            try:
                data = imread(f)
                if (len(shape(data))==3):
                    data = data[:,:,0]

            except:
                print "couldnt open image with imread"
                raise Exception()

        return data


# TODO change that to libtiff
# e.g.
# tif = TIFFfile('../../Data/wingdisc-stack-grayscale.tiff')
# data = tif.get_samples()[0][0]

def calcMSE(x0,x):
    return np.mean((x0-x)**2)

    return np.mean((x0-x)**2) + lam*np.mean(TV_norm(x0-x)**2)


def calcPSNR(x0,x, shiftMean = False):
    # return 10*(2*np.log10((np.amax(x0)-np.amin(x0)))-\
    #                       np.log10(np.mean((x0-x)**2)))

    if shiftMean:
        dx = np.mean(x0) - np.mean(x)
    else:
        dx = 0

    return 20*np.log10(np.amax(x0)-np.amin(x0))-10*np.log10(np.mean((x0-x-dx)**2))



if __name__ == '__main__':
    # createFakeSpimFolder("Fake", createData = True)
    # d = fromSpimFolder("Fake",count=1)
    # print d.shape

    d = fromSpimFolder("/Users/mweigert/Data/TEST")
