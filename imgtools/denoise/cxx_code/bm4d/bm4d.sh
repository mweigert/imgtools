#! /bin/sh

PWD0=$(pwd)
MATLABDIR=/Users/mweigert/Documents/MATLAB/MyBM4D/

if [ $# -le 2 ]
then
    echo "to few arguments supplied!"
	echo "\nbm4d infile outfile sigma\n"
	echo "in/outfile have to be given with abolsute paths!"
else
	cd $MATLABDIR
	echo $1 $2 $3
	./run_mybm4d.sh /Applications/MATLAB_R2014a.app/ $1 $2 $3 
fi


