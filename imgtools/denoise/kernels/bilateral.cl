
__kernel void run2d_short(__read_only image2d_t input, __global short* output,const int Nx, const int Ny,const int fSize, const float sigmaX,const float sigmaP)
{
  const sampler_t sampler = CLK_NORMALIZED_COORDS_FALSE |	CLK_ADDRESS_CLAMP_TO_EDGE |	CLK_FILTER_NEAREST ;

  uint i = get_global_id(0);
  uint j = get_global_id(1);

  float res = 0;
  float sum = 0;
  uint pix0 = read_imageui(input,sampler,(int2)(i,j)).x;
  
  for(int k = -fSize;k<=fSize;k++){
    for(int m = -fSize;m<=fSize;m++){

    uint pix1 = read_imageui(input,sampler,(int2)(i+k,j+m)).x;
    float weight = exp(-1.f/sigmaX/sigmaX*(k*k+m*m))*
	  exp(-1.f/sigmaP/sigmaP*((1.f*pix0-pix1)*(1.f*pix0-pix1)));
    res += pix1*weight;
    sum += weight;

    }
  }

  output[i+Nx*j] = (short)(res/sum);
}

__kernel void run2d_float(__read_only image2d_t input, __global float* output,const int Nx, const int Ny,const int fSize, const float sigmaX,const float sigmaP)
{
  const sampler_t sampler = CLK_NORMALIZED_COORDS_FALSE |	CLK_ADDRESS_CLAMP_TO_EDGE |	CLK_FILTER_NEAREST ;

  uint i = get_global_id(0);
  uint j = get_global_id(1);

  float res = 0;
  float sum = 0;
  float pix0 = read_imagef(input,sampler,(int2)(i,j)).x;
  
  for(int k = -fSize;k<=fSize;k++){
    for(int m = -fSize;m<=fSize;m++){

    float pix1 = read_imagef(input,sampler,(int2)(i+k,j+m)).x;
    float weight = exp(-1.f/sigmaX/sigmaX*(k*k+m*m))*
	  exp(-1.f/sigmaP/sigmaP*(1.f*pix0-pix1)*(1.f*pix0-pix1));
    res += pix1*weight;
    sum += weight;

    }
  }

  output[i+Nx*j] = res/sum;
}


__kernel void run3_short(__read_only image3d_t input, __global short* output,const int Nx, const int Ny,const int fSize, const float sigma)
{

    const sampler_t sampler = CLK_NORMALIZED_COORDS_FALSE |	CLK_ADDRESS_CLAMP_TO_EDGE |	CLK_FILTER_NEAREST ;

  uint i = get_global_id(0);
  uint j = get_global_id(1);
  uint k = get_global_id(2);
  

  uint pix0 = read_imageui(input,sampler,(int4)(i,j,k,0)).x;
  
  float res = 0;
  float sum = 0;


  for(int i2 = -fSize;i2<=fSize;i2++){
	for(int j2 = -fSize;j2<=fSize;j2++){
	  for(int k2 = -fSize;k2<=fSize;k2++){
	
		uint pix1 = read_imageui(input,sampler,(int4)(i+i2,j+j2,k+k2,0)).x;
		float weight = exp(-1.f/sigma/sigma*(i2*i2+j2*j2+k2*k2));
		res += pix1*weight;
		sum += weight;
	  }
	}
  }
  
  output[i+j*Nx+k*Nx*Ny] = (short)(res/sum);

}

__kernel void run3_float(__read_only image3d_t input, __global float* output,const int Nx, const int Ny,const int fSize, const float sigma)
{

    const sampler_t sampler = CLK_NORMALIZED_COORDS_FALSE |	CLK_ADDRESS_CLAMP_TO_EDGE |	CLK_FILTER_NEAREST ;

  uint i = get_global_id(0);
  uint j = get_global_id(1);
  uint k = get_global_id(2);
  

  float pix0 = read_imagef(input,sampler,(int4)(i,j,k,0)).x;
  
  float res = 0;
  float sum = 0;


  for(int i2 = -fSize;i2<=fSize;i2++){
	for(int j2 = -fSize;j2<=fSize;j2++){
	  for(int k2 = -fSize;k2<=fSize;k2++){
	
		float pix1 = read_imagef(input,sampler,(int4)(i+i2,j+j2,k+k2,0)).x;
		float weight = exp(-.01f*(i2*i2+j2*j2+k2*k2))*
		  exp(-1.f/sigma/sigma*((1.f*pix0-pix1)*(1.f*pix0-pix1)));

		res += pix1*weight;
		sum += weight;
	  }
	}
  }
  
  output[i+j*Nx+k*Nx*Ny] = res/sum;

}


__kernel void run2dBuf(__global short *  input, __global short* output,const int Nx, const int Ny,const int fSize, const float sigmaX,const float sigmaP)
{
  const sampler_t sampler = CLK_NORMALIZED_COORDS_FALSE |	CLK_ADDRESS_CLAMP_TO_EDGE |	CLK_FILTER_NEAREST ;

  uint i = get_global_id(0);
  uint j = get_global_id(1);

  float res = 0;
  float sum = 0;
  uint pix0 = input[i+Nx*j];
  
  for(int k = -fSize;k<=fSize;k++){
    for(int m = -fSize;m<=fSize;m++){


	uint pix1 = input[(i+k)+Nx*(j+m)];
  
	float weight = exp(-1.f/sigmaX/sigmaX*(k*k+m*m))*
	  exp(-1.f/sigmaP/sigmaP*((1.f*pix0-pix1)*(1.f*pix0-pix1)));
    res += pix1*weight;
    sum += weight;

    }
  }

  output[i+Nx*j] = (short)(res/sum);
}

