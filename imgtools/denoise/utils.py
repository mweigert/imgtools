import os
import numpy as np


def absPath(s):
    return os.path.join(os.path.dirname(__file__),s)


def _extended_slice(slice0,N,Nmargin):
    """ returns

    slice1, slice2         where

    slice1   -   slice0 extended by +/- Nmargin
    slice2   -   s.t. data[slice1][slice2] = data[slice0]
    """

    dx1 = Nmargin+min(0,slice0.start-Nmargin)
    dx2 = min(N,slice0.stop+Nmargin) - slice0.stop

    slice1 = slice(slice0.start-dx1,slice0.stop+dx2)
    slice2 = slice(dx1,slice1.stop-slice1.start-dx2)
    return slice1, slice2



def derivX(x,mode="symmetric"):
    return .5*(np.pad(x,((0,0),(0,1)),mode)[:,1:]-\
               np.pad(x,((0,0),(1,0)),mode)[:,:-1])

def derivY(x,mode="symmetric"):
    return .5*(np.pad(x,((0,1),(0,0)),mode)[1:,:]-\
               np.pad(x,((1,0),(0,0)),mode)[:-1,:])

def TV_norm(x):
    TV_x = derivX(x)
    TV_y = derivY(x)
    return np.sqrt(TV_x**2+TV_y**2)

def calcMSE(x0,x):
    return np.mean((x0-x)**2)

    return np.mean((x0-x)**2) + lam*np.mean(TV_norm(x0-x)**2)


def calcPSNR(x0,x, shiftMean = False):
    # return 10*(2*np.log10((np.amax(x0)-np.amin(x0)))-\
    #                       np.log10(np.mean((x0-x)**2)))

    if shiftMean:
        dx = np.mean(x0) - np.mean(x)
    else:
        dx = 0
        
    return 20*np.log10(np.amax(x0)-np.amin(x0))-10*np.log10(np.mean((x0-x+dx)**2))
