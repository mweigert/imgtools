# imgtools

Image processing algorithms in OpenCL

## Installing

On Mac, clang might complain about non supported compiler flags, a quick fix to which is

> export CFLAGS=-Qunused-arguments
> export CPPFLAGS=-Qunused-arguments

then install via pip

> pip install --user git+http://mweigert@bitbucket.org/mweigert/pyocl

> pip install --user git+http://mweigert@bitbucket.org/mweigert/imgtools


test with

> python -m imgtools


## Usage


###convolutions:

    2D
		convolve2
		convolve_sep2

    3D
		convolve3
		convolve_sep3

	fftconvolve

Example

	:::python

   	from imgtools import convolve_sep3, fftconvolve

   	result1 = convolve_sep3(data,hx,hy,hz)
   	result2 = fftconvolve(data,einsum("i,j,k",hx,hy,hz))   #should be the same


###denoising filters in 2d and 3d:

As of now, the following methods are available:
   
    2D
		bilateral
		dct_8x8
    	nlm
		nlm_fast

	3D
		bilateral3
		nlm3
		nlm3_fast
		tv3_gpu
		wiener3

Example:

	:::python

   	from imgtools.denoise import nlm_fast, nlm_fast3, tv3_gpu

   	result2d = nlm_fast(data,2,3)
	
   	result3d = tv3_gpu(data,2.)

###steerable filters:

	:::python

	from imgtools.imgutils.test_images import mpi_logo3
	
	from imgtools import SteerableFilter3
	
   	data = mpi_logo3().astype(float32)

   	s = SteerableFilter3()

   	s.run(data, sigma = 4., Ncut = 1)


	response  = s.responseMax
	phi  = s.phiMax
	theta  = s.thetaMax


### imgutils

	:::python

   	from imgtools.imgutils import read3dTiff, write3dTiff



## setting the GPU to be used:

 put a config file ".imgtools" in your home folder

    OPENCLDEVICE = 1